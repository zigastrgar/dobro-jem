<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAArHnTL8znGX6fXFmZi5YzgjfkQrD8IH0&callback=initMap"></script>
<script type="text/javascript">
    var map;
    function initMap() {
        map = new google.maps.Map(document.getElementById('restaurant-map'), {
            center: {lat: {{ $restaurant->lat }}, lng: {{ $restaurant->lng }}},
            zoom: 17,
            disableDefaultUI: false,
            scrollwheel: false
        });

        infowindow = new google.maps.InfoWindow({
            content: "<p>{{ $restaurant->name }}</p><p>{{ $restaurant->address }}</p>"
        });

        marker = new google.maps.Marker({
            position: {lat: {{ $restaurant->lat }}, lng: {{ $restaurant->lng }}},
            map: map,
            icon: '{{ mapMarkerColor(getStateOfRestaurant($restaurant)) }}'
        });

        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map,marker);
        });
    }
</script>