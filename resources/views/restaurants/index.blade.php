@extends('app')

@section('content')
    <div class="col-xs-12">
        <div class="page-header text-center">
            <h1>Restavracije</h1>
            @include('restaurants.partials.dropdown')
        </div>
    </div>
    <div id="restaurants">
        @if(count($restaurants) > 0)
            @foreach($restaurants as $restaurant)
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    @include('restaurants.partials.restaurant_list')
                </div>
            @endforeach
            <div class="col-xs-12 text-center">{{ $restaurants->appends(Request::except('page'))->links() }}</div>
        @endif
    </div>
@stop

@section('scripts')
    <script src="./assets/vendor/select/js/bootstrap-select.js"></script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
            $(document).on('change', 'select[name=order]', function (e) {
                window.location.replace("{{ generateUrl(url()->current(), Request::except('order')) }}&order=" + e.target.value);
            });
        })
    </script>
    @include('restaurants.partials.like_script')
@stop