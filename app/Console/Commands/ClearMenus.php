<?php

namespace App\Console\Commands;

use App\Menu;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearMenus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:menus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes old menus form the database';

    protected $menu;

    /**
     * Create a new command instance.
     *
     * @param \App\Console\Commands\Menu|\App\Menu $menu
     */
    public function __construct(Menu $menu)
    {
        parent::__construct();

        $this->menu = $menu;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::now()->toDateString();
        DB::table('menus')->where('created_at', '<', $today . " 00:00:00")->delete();
    }
}
