@extends('app')

@section('content')
    <h1 class="page-header text-center">Piškoti na naši strani</h1>
    <div class="col-xs-12 col-sm-8 col-sm-push-2">
        <h2 class="text-center">Jap, poleg vseh ponujenih restavracij tudi mi uporabljamo piškotke, ampak žal ne takšne kot jih pečejo naše babice :)</h2>
        <table class="table table-bordered table-responsive">
            <tr>
                <th>Ime</th>
                <th>Namen</th>
                <th>Lastnik</th>
                <th>Čas trajanja</th>
            </tr>
            <tr>
                <td>laravel_session</td>
                <td>Je piškotek, ki se uporablja za identifikacijo seje v Laravelu</td>
                <td>Dobro jem</td>
                <td>1 ura</td>
            </tr>
            <tr>
                <td>XSRF-TOKEN</td>
                <td>Piškotek, ki se uporablja za preprečevanje CSRF (Cross-site request forgery) napadaov</td>
                <td>Dobro jem</td>
                <td>1 ura</td>
            </tr>
            <tr>
                <td>remember_web_*</td>
                <td>Piškotek, ki služi avtorizaciji uporabnika</td>
                <td>Dobro jem</td>
                <td>5 let</td>
            </tr>
            <tr>
                <td>__ga, __gat</td>
                <td>Statistika ogledv spletne strani</td>
                <td>Google</td>
                <td>Različno</td>
            </tr>
        </table>
        <h3 class="page-header">Kaj so piškoti?</h3>
        <p>Piškotek je majhna datoteka, ki se shrani na računalniku ob obisku spletne strani. Piškotki vsebujejo različne informacije, ki jih spletna stran prebere, ko jo ponovno obiščete. Uporablja jih večina sodobnih spletnih strani.
            <a href="http://eur-lex.europa.eu/legal-content/SL/TXT/HTML/?uri=CELEX:32002L0058&from=SL" target="_blank">Preberi več</a></p>
    </div>
@stop