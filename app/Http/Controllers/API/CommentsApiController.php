<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;

class CommentsApiController extends ApiController
{
    public function comment(Request $request)
    {
        if ( Auth::loginUsingId($request->input('user')) ) {
            $comment = new Comment([
                'restaurant_id' => $request->input('restaurant'),
                'content'       => $request->input('comment')
            ]);
            Auth::user()->comments()->save($comment);

            return $this->respondNoContent();
        }

        return $this->respondNotFound("Težava z uporabniškim ID-jem!");
    }
}
