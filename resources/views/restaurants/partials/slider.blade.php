{!! Form::open(['url' => 'lucky', 'method' => 'GET', 'class' => 'col-xs-12 col-sm-8 col-sm-push-2 col-md-6 col-md-push-3', 'style' => 'margin-top: 25px;']) !!}
<p data-toggle="tooltip" data-html="true" data-placement="bottom"
   title="Vaša lokacija mogoče ni natančna, za natančno lokacijo se prijavite in vnesite naslov!" class="text-center">
    Iskanje naključne
    restavracije znotraj {{ $distance }} metrov od vaše lokacije.</p>
<div class="form-group">
    <input type="slider" id="slider" name="meters" value="{{ $distance }}">
</div>
<div style="margin-top: 30px;" class="form-group">
    <button type="submit" class="btn btn-brand center-block"><span
                class="icon icon-help"></span>
        Naključna restavracija
    </button>
</div>
{!! Form::close() !!}