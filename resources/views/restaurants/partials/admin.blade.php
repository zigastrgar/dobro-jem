@if(Auth::check() && Auth::user()->status == 'admin')
    <div class="col-xs-12 text-center">
        <a class="btn btn-danger" href="{{ action('Admin\AdminController@editImages', ['slug' => $restaurant->slug]) }}">Uredi slike</a>
    </div>
@else

@endif