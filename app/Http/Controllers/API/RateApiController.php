<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;

class RateApiController extends ApiController
{
    public function rate(Request $request)
    {
        $rate = $request->input('rate');
        if ( $rate > 5 ) {
            $rate = 5;
        } else if ( $rate < 0 && !is_null($rate) ) {
            return $this->respondWithValidationErrors("Težava z oceno!");
        }

        if ( Auth::loginUsingId($request->input('user')) ) {
            if ( !didRateRestaurant($request->input('user'), $request->input('restaurant'), $request->input('type')) ) {
                $rating = new Rating([
                    'restaurant_id'  => $request->input('restaurant'),
                    'rating_type_id' => getRateTypeId($request->input('type')),
                    'rate'           => $rate
                ]);
                Auth::user()->ratings()->save($rating);
                $json["response"] = true;
                $json["message"]  = "Ocena uspešno dodana!";
            } else {
                $json["response"] = false;
                $json["message"]  = "Uporabnik je že ocenil to restavracijo za ta tip ocene!";
            }

            return $this->respond($json);
        }

        return $this->respondNotFound("Težava z uporabniškim ID-jem!");
    }
}
