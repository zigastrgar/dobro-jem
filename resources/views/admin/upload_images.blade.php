@extends("app")

@section('content')
    <div class="col-xs-12">
        <h1 class="page-header text-center">Upload</h1>
    </div>
    <div id="upl">
<form action="upload_image/{{ $id }}" id="upload" enctype="multipart/form-data">
<input type="file" name="file[]" multiple ><br />
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="submit">
</form>
        <script>
            var form = document.getElementById('upload');
            var request = new XMLHttpRequest();
            form.addEventListener('submit',function(e){
                e.preventDefault();
                var formdata=new FormData(form)
                request.open('post','upload_image/{{ $id }}');
                request.addEventListener("load", transferComplete)
                request.send(formdata);
            });

            function  transferComplete(data){
               console.log(data.currentTarget.response);
            }
        </script>
    </div>
@stop