<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_logs', function(Blueprint $table){
            $table->increments('id');
            $table->string('key');
            $table->foreign('key')->references('key')->on('api_keys');
            $table->string('page');
            $table->text('additional')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('api_logs', function(Blueprint $table){
            $table->dropForeign('api_logs_key_foreign');
        });

        Schema::drop('api_logs');
    }
}
