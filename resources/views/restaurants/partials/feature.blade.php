<div style="width: 55px; display: inline-block;" data-toggle="tooltip" data-html="true" data-placement="top" title="{{ $feature->name }}">
    <div class="product-chooser-item">
        <span class="icon icon-{{ $feature->picture }} font-1-3-x"></span>
        <div class="col-xs-12">
            <input type="checkbox" name="feature[]" value="{{ $feature->id }}">
        </div>
        <div class="clear"></div>
    </div>
</div>