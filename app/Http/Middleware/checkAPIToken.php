<?php

namespace App\Http\Middleware;

use App\Api;
use Carbon\Carbon;
use Closure;

class checkAPIToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Api::where('key', $request->input('api_key', 'string'))->where('valid_until', '>', Carbon::now())->exists()){
            return $next($request);
        } else {
            return abort(403, 'Nedovoljen zahtevek!');
        }
    }
}
