<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportType extends Model
{
    protected $table = 'report_types';

    protected $fillable = [ 'title' ];

    public function reports()
    {
        return $this->hasMany('App\Report');
    }
}
