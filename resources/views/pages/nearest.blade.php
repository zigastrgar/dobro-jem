@extends('app')

@section('content')
    <h1 class="page-header text-center">Restavracije blizu mene</h1>
    @include('pages.partials.map')
@stop

@section('scripts')
    @include('pages.scripts.map_scripts')
@stop