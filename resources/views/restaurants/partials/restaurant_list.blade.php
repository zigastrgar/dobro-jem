<div class="restaurant">
    <div class="rest">
        <div class="features">
            <?php
            $arr = $restaurant->features->toArray(); shuffle($arr); ?>
            @foreach($arr as $feature)
                @include('restaurants.partials.feature_restaurant_list')
            @endforeach
        </div>
        <div class="info">
            <h2>
                <div class="activity {{ transformStateToClass(getStateOfRestaurant($restaurant)) }}"></div>
                <a href="{{ action('RestaurantsController@show', [$restaurant->slug]) }}">{{ $restaurant->name }}</a>
                <br/>
                <small>{{ $restaurant->address.", ".$restaurant->city }}</small>
            </h2>
        </div>
        <div class="price-area">
            <div class="price action-price">{{ price($restaurant->value_of_charge, "€") }}</div>
        </div>
        <div class="additional">
            <div class="item" data-toggle="tooltip" data-html="true" data-placement="top"
                 title="Povprečna ocena restavracije. Najvišja možna ocena je 5">
                <p class="header"><span class="icon icon-star"></span></p>
                <p class="body">{{ getRating($restaurant->ratings()->lists('rate')) }}</p>
            </div>
            <div class="item" data-toggle="tooltip" data-html="true" data-placement="top"
                 title="Pri izračunu oddaljenosti lahko pride do napake">
                <p class="header"><span class="icon-ios-circle-filled"></span></p>
                <p class="body">
                    @if(Auth::check() && strlen(Auth::user()->lat) > 0 && strlen(Auth::user()->lng))
                        {{ humanizeDistance(calculateDistance($restaurant->lat, $restaurant->lng, Auth::user()->lat, Auth::user()->lng)) }}
                    @else
                        {{ humanizeDistance(calculateDistance($restaurant->lat, $restaurant->lng, session('lat'), session('lng'))) }}
                    @endif
                </p>
            </div>
            <div class="item">
                <p class="header"><span class="icon icon-comment"></span></p>
                <p class="body">{{ count($restaurant->comments) }}</p>
            </div>
        </div>
    </div>
</div>