@extends('app')

@section('content')
    <div class="col-xs-12">
        <h1 class="page-header text-center">
            Več o uporabniku
            <small>{{ $user->name }}</small>
        </h1>
        <h2 class="page-header text-center">Komentarji ({{ count($user->comments->toArray()) }})</h2>
        @if(count($user->comments->toArray()) > 0)
            <table class="table table-responsive table-bordered table-condensed">
                <tr>
                    <th>Vsebina</th>
                    <th>Datum</th>
                    <th>Restavracija</th>
                </tr>
                @foreach($user->comments as $comment)
                    <tr>
                        <td>{{ $comment->content }}</td>
                        <td>{{ $comment->created_at }}</td>
                        <td><a href="{{ action('RestaurantsController@show', ['slug' => $comment->restaurant->slug]) }}">{{ $comment->restaurant->name }}</a></td>
                    </tr>
                @endforeach
            </table>
        @endif
        <h2 class="page-header text-center">Priljubljene ({{ count($user->likes->toArray()) }})</h2>
        @if(count($user->likes->toArray()) > 0)
            <table class="table table-responsive table-bordered table-condensed">
                <tr>
                    <th>Restavracija</th>
                    <th>Datum</th>
                </tr>
                @foreach($user->likes as $favourite)
                    <tr>
                        <td><a href="{{ action('RestaurantsController@show', ['slug' => $favourite->restaurant->slug]) }}">{{ $favourite->restaurant->name }}</a></td>
                        <td>{{ $favourite->created_at }}</td>
                    </tr>
                @endforeach
            </table>
        @endif
        <h2 class="page-header text-center">Ocene ({{ count($user->ratings->toArray()) }})</h2>
        @if(count($user->ratings->toArray()) > 0)
            <table class="table table-responsive table-bordered table-condensed">
                <tr>
                    <th>Restavracija</th>
                    <th>Tip</th>
                    <th>Ocena</th>
                    <th>Datum</th>
                </tr>
                @foreach($user->ratings as $rating)
                    <tr>
                        <td><a href="{{ action('RestaurantsController@show', ['slug' => $rating->restaurant->slug]) }}">{{ $rating->restaurant->name }}</a></td>
                        <td>{{ $rating->type->name }}</td>
                        <td>{{ $rating->rate }}</td>
                        <td>{{ $rating->created_at }}</td>
                    </tr>
                @endforeach
            </table>
        @endif
    </div>
@endsection