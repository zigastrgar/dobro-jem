<?php
use Illuminate\Support\Facades\DB;

/**
 * Returns day type of given date
 *
 * @param string $date
 *
 * @return string
 */
function getTypeOfDay($date = "")
{
    $date2 = date("l");

    if ( $date != "" )
        $date2 = date("l", strtotime($date));

    switch ( $date2 ) {
        case "Monday":
            $dayType = "work";
            break;
        case "Tuesday":
            $dayType = "work";
            break;
        case "Wednesday":
            $dayType = "work";
            break;
        case "Thursday":
            $dayType = "work";
            break;
        case "Friday":
            $dayType = "work";
            break;
        case "Saturday":
            $dayType = "saturday";
            break;
        case "Sunday":
            $dayType = "sunday";
            break;
    }

    return $dayType;
}

/**
 * From parameter returns stars for rating
 *
 * @param $number
 *
 * @return string
 */
function showStars($number)
{
    $stars = "";
    $i     = 0;
    for ( $i; $i < (int)$number; $i++ ) {
        $stars .= "<span class='icon icon-star'></span>";
    }
    $rest = $number - (int)$number;
    if ( $rest >= 0.3 && $rest <= 0.7 && $i < 5 ) {
        $stars .= "<span class='icon icon-star-half-o'></span>";
        $i++;
    } else if ( $rest > 0.7 && $i < 5 ) {
        $stars .= "<span class='icon icon-star'></span>";
        $i++;
    } else if ( $rest < 0.3 && $i < 5 ) {
        $stars .= "<span class='icon icon-star-o'></span>";
        $i++;
    }
    while ( $i < 5 ) {
        $stars .= "<span class='icon icon-star-o'></span>";
        $i++;
    }

    return $stars;
}

/**
 * Convert's string to id
 *
 * @param $string
 *
 * @return int
 */
function getRateTypeId($string)
{
    switch ( $string ) {
        case "quality":
            $id = 1;
            break;
        case "service":
            $id = 3;
            break;
        case "ambient":
            $id = 4;
            break;
        case "clean":
            $id = 2;
            break;
    }

    return $id;
}

/**
 * Returns calculated rating of given restaurant
 *
 * @param $array
 *
 * @return float
 */
function getRating($array = [ ])
{
    $array = $array->toArray();
    $i = count($array);

    return round($i != 0 ? array_sum($array) / $i : 0, 2);
}

/**
 * Returns image name for restaurants
 *
 * @return mixed
 */
function randomImage()
{
    $images = [
        "food-dinner-grilled-shashlik.jpg",
        "city-restaurant-lunch-outside.jpg",
        "food-salad-healthy-vegetables.jpg",
        "black-and-white-restaurant-street-walking.jpg",
        "food-kitchen-cutting-board-cooking.jpg",
        "food-pizza-restaurant-eating.jpg",
        "food-plate-wood-restaurant.jpg",
        "food-pot-kitchen-cooking.jpg",
        "food-salad-healthy-lunch.jpg",
        "food-salad-restaurant-person.jpg",
        "food-vegetables-italian-restaurant.jpg",
        "menu-restaurant-vintage-table.jpg",
        "pexels-photo.jpg",
        "restaurant-man-bar-building.jpg",
        "restaurant.jpg"
    ];

    return $images[rand(1, count($images)) - 1];
}

/**
 * Return state of the restaurant
 *
 * @param $data
 *
 * @return bool|null
 */
function getStateOfRestaurant($data)
{
    $type = getTypeOfDay();
    if ( $type == "work" ) {
        return !empty( $data->opening_week ) ? isOpened(explode(",", $data->opening_week)) : null;
    } else if ( $type == "saturday" ) {
        return !empty( $data->opening_sat ) ? isOpened(explode(",", $data->opening_sat)) : null;
    }

    return !empty( $data->opening_sun ) ? isOpened(explode(",", $data->opening_sun)) : null;
}

/**
 * Determinate if restaurant is opened or not
 *
 * @param $hours
 *
 * @return bool
 */
function isOpened($hours)
{
    $currentTime = date("H:i");

    return $currentTime >= $hours[0] && $currentTime <= $hours[1] ? true : false;
}

/**
 * Translate correct state of restaurant to corespondent CSS class
 *
 * @param $state
 *
 * @return string
 */
function transformStateToClass($state)
{
    if ( $state ) {
        return "opened";
    } else if ( is_null($state) ) {
        return "undefined";
    }

    return "closed";
}

/**
 * Returns correct time of work for given day
 *
 * @param $data
 *
 * @return null
 */
function returnTimeOfWork($data)
{
    $type = getTypeOfDay();
    if ( $type == "work" ) {
        return !empty( $data->opening_week ) ? $data->opening_week : null;
    } else if ( $type == "saturday" ) {
        return !empty( $data->opening_sat ) ? $data->opening_sat : null;
    }

    return !empty( $data->opening_sun ) ? $data->opening_sun : null;
}

/**
 * Format date
 *
 * @param $data
 *
 * @return string
 */
function formatDate($data)
{
    if ( is_null($data) || empty( $data ) )
        return "Ni podatka";
    $data = explode(",", $data);

    return $data[0] . " - " . $data[1];
}

/**
 * Returns correct color for map marker
 *
 * @param $state
 *
 * @return string
 */
function mapMarkerColor($state)
{
    $color = "red.png";
    if ( $state ) {
        $color = "green.png";
    } else if ( is_null($state) ) {
        $color = "orange.png";
    }

    return "/assets/img/" . $color;
}

/**
 * Converts Eloquent model to JS Array
 *
 * @param $restaurants
 *
 * @return string
 */
function restaurantsToJSArray($restaurants)
{
    $array = [ ];
    foreach ( $restaurants as $restaurant ) {
        $name        = str_replace('\'', "", $restaurant->name);
        $address     = $restaurant->address;
        $lat         = $restaurant->lat;
        $lng         = $restaurant->lng;
        $id          = $restaurant->slug;
        $price       = price($restaurant->value_of_charge, "€");
        $markerColor = mapMarkerColor(getStateOfRestaurant($restaurant));
        $array[]     = "['$name', '$address', $lat, $lng, '$id', '$markerColor', '$price']";
    }

    return implode(",", $array);
}

/**
 * Returns correct price format with unite
 *
 * @param      $price
 * @param null $unite
 *
 * @return string
 */
function price($price, $unite = null)
{
    if ( !is_null($unite) )
        return number_format(trim($price), 2, ",", ".") . " $unite";

    return number_format(trim($price), 2, ",", ".");
}

/**
 * Translates physical address to geo location
 *
 * @param $address
 *
 * @return array
 */
function addressToGeoLocation($address)
{
    $geo =
        json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address)), true);

    return [
        'lat' => $geo['results'][0]['geometry']['location']['lat'],
        'lng' => $geo['results'][0]['geometry']['location']['lng']
    ];
}

/**
 * Parse phone number
 *
 * @param $phone
 *
 * @return string
 */
function phoneParse($phone)
{
    $phone = str_replace(" ", "", $phone);
    if ( starts_with($phone, "041") || starts_with($phone, "031") || starts_with($phone, "051") || starts_with($phone, "070") || starts_with($phone, "040") ) {
        return substr($phone, 0, 3) . " " . substr($phone, 3, 3) . " " . substr($phone, 6, 3);
    } else if ( strlen($phone) == 0 ) {
        return "Ni podatka";
    }

    return substr($phone, 0, 2) . " " . substr($phone, 2, 3) . " " . substr($phone, 5, 2) . " " . substr($phone, 7, 2);
}

/**
 * Prepare string for tooltip
 *
 * @param $numbers
 *
 * @return string
 */
function otherPhoneNumbers($numbers)
{
    $numbers = explode(",", $numbers);
    $i       = 1;
    $numbs   = [ ];
    while ( $i < count($numbers) ) {
        $numbs[] = phoneParse($numbers[$i]);
        $i++;
    }

    return implode("<br />", $numbs);
}

/**
 * Returns calculated distance between person and given restaurant in meters
 *
 * @param $lat
 * @param $lng
 * @param $rest_lat
 * @param $rest_lng
 *
 * @return float
 */
function calculateDistance($lat, $lng, $rest_lat, $rest_lng)
{
    $lat      = deg2rad($lat);
    $lng      = deg2rad($lng);
    $rest_lat = deg2rad($rest_lat);
    $rest_lng = deg2rad($rest_lng);
    if ( is_null($lat) || is_null($lng) )
        return null;

    $distance = acos(cos($lat) * cos($rest_lat) * cos($lng - $rest_lng) + sin($lat) * sin($rest_lat)) * 6371000;

    return ceil($distance / 5) * 5;
}

/**
 * Humanize distance
 *
 * @param $distance
 *
 * @return string
 */
function humanizeDistance($distance)
{
    $distance = ceil($distance / 5) * 5;
    if ( is_null($distance) ) {
        return "Ni podatka!";
    } else if ( $distance > 1000 ) {
        return number_format($distance / 1000, 2, ",", ".") . " km";
    }

    return $distance . " m";
}

/**
 * Generates URL from current requests
 *
 * @param $url
 * @param $requests
 *
 * @return string
 */
function generateUrl($url, $requests)
{
    return $url . "?" . http_build_query($requests);
}

/**
 * Determinate if user did rate specific restaraunt and type
 *
 * @param $user
 * @param $restaurant
 * @param $type
 *
 * @return bool
 */
function didRateRestaurant($user, $restaurant, $type)
{
    if ( is_null($user) || $user == 0 )
        return true;
    $type   = getRateTypeId($type);
    $select = DB::table('ratings')->where('user_id', $user)->where('restaurant_id', $restaurant)
        ->where('rating_type_id', $type)->get();

    return count($select) < 1 ? false : true;
}

/**
 * Determinate if user liked the restaurant
 *
 * @param $user
 * @param $restaurant
 *
 * @return bool
 */
function liked($user, $restaurant)
{
    if ( is_null($user) )
        return false;
    $select = DB::table('favourites')->where('user_id', $user)->where('restaurant_id', $restaurant)->get();

    return count($select) == 1 ? true : false;
}

/**
 * Reslove IP into Geolocation
 *
 * @param $ip
 *
 * @return array
 */
function ip2Geo($ip)
{
    $geo = json_decode(file_get_contents("http://ip-api.com/json/" . urlencode($ip)), true);

    return [ 'lat' => $geo['lat'], 'lng' => $geo['lon'] ];
}

/**
 * Used for indexing
 *
 * @param $string
 *
 * @return string
 */
function translate($string)
{
    return str_replace('ć', 'c', str_replace('š', 's', str_replace('č', 'c', str_replace('ž', 'z', trim(mb_strtolower($string))))));
}

/**
 * Clear string up a bit
 *
 * @param $string
 *
 * @return mixed
 */
function clear($string)
{
    return str_replace('´', '', str_replace("'", "", str_replace("gostilna ", "", str_replace("restavracija ", "", str_replace("okrepcevalnica ", "", str_replace("č", "c", str_replace("ć", "c", str_replace("ž", "z", str_replace("š", "s", $string)))))))));
}

/**
 * Removes pointless words from restaurant name
 *
 * @param $name
 *
 * @return string
 */
function smartName($name)
{
    if ( !preg_match("/^Okrepcevalnica ([a-z]|sarajevskih|in|ter|pri|[0-9]+)/", $name) ) {
        return ucfirst(trim(str_replace("Okrepcevalnica ", "", $name)));
    } else {
        return ucfirst(trim($name));
    }
}

/**
 * Main function for getting time remaining until opening or closing restaurant
 *
 * @param $restaurant
 *
 * @return null|string
 */
function timeRemaining($restaurant)
{
    $state = getStateOfRestaurant($restaurant);

    if ( is_null($state) )
        return null;

    if ( ! $state ) {
        $nextDay = date("Y-m-d") . " 23:59";
        if ( date("Y-m-d H:i") < $nextDay && date("H:i") > getTimeOfOpening($restaurant) ) {
            $dayType = getTypeOfDay("+1days");
        } else {
            $dayType = getTypeOfDay();
        }
    } else {
        $dayType = getTypeOfDay();
    }

    switch ($dayType) {
        case "work":
            $time = explode(",", $restaurant->opening_week);
            break;
        case "saturday":
            $time = explode(",", $restaurant->opening_sat);
            break;
        case "sunday":
            $time = explode(",", $restaurant->opening_sun);
            break;
    }

    if ( !empty($time[0]) ) {
        $time[1] = date("Y-m-d") . " " . $time[1];
        if ( strtotime(date("H:i")) > strtotime(getTimeOfOpening($restaurant)) ) {
            $time[0] = date("Y-m-d", strtotime("+1days")) . " " . $time[0];
        } else {
            $time[0] = date("Y-m-d") . " " . $time[0];
        }
        if ( $state ) {
            $theirTime = strtotime($time[1]);
            $text = "zaprtja";
        } else {
            $theirTime = strtotime($time[0]);
            $text = "odprtja";
        }
        $difference = abs($theirTime - strtotime(date("H:i")));

        return "Do $text še: " . humanizeDifference($difference);
    } else {
        return null;
    }
}

/**
 * Returns opening time for specific restaurant today
 *
 * @param $restaurant
 *
 * @return mixed
 */
function getTimeOfOpening($restaurant)
{
    switch ( getTypeOfDay() ) {
        case "work":
            $time = explode(",", $restaurant->opening_week);
            break;
        case "saturday":
            $time = explode(",", $restaurant->opening_sat);
            break;
        case "sunday":
            $time = explode(",", $restaurant->opening_sun);
            break;
    }

    return $time[0];
}

/**
 * Transforms seconds into human readable time
 *
 * @param $seconds
 *
 * @return string
 */
function humanizeDifference($seconds)
{
    $hours   = (int)( $seconds / ( 60 * 60 ) );
    $seconds = $seconds - (int)$hours * 3600;
    $minutes = $seconds / 60;
    $string  = "";
    if ( $hours != "0" ) {
        $string .= $hours . "h ";
    }
    $string .= $minutes . "min";

    return $string;
}

/**
 * Function which converts name to correct form for ordering
 *
 * @param $name
 *
 * @return mixed
 */
function translateName($name)
{
    return str_replace('´', '', str_replace('`', '', str_replace('/', '-', str_replace('--', '-', str_replace('\'', "", str_replace("&", "", str_replace(".", "", str_replace(",", "", str_replace(" ", "-", str_replace("-", "", str_replace('Š', 's', str_replace('Ž', 'z', str_replace('Č', 'c', str_replace("š", "s", str_replace("č", "c", str_replace('ž', 'z', trim(strtolower($name))))))))))))))))));
}

/**
 * Function to clean up the mess after ordering
 *
 * @param $array
 * @param $keys
 *
 * @return mixed
 */
function cleanUp($array, $keys)
{
    foreach ( $keys as $key ) {
        $i = 0;
        while ( $i < count($array) ) {
            unset( $array[$i][$key] );
            $i++;
        }
    }

    return $array;
}

/**
 * Function to sort by distance
 *
 * @param      $a
 * @param      $b
 *
 * @return mixed
 * @internal param bool $order
 *
 */
function sortByDistance($a, $b)
{
    return $a['distance_metres'] - $b['distance_metres'];
}

/**
 * @param $a
 * @param $b
 *
 * @return mixed
 */
function sortByDistanceDesc($a, $b)
{
    return $b['distance_metres'] - $a['distance_metres'];
}

/**
 * Function to sort by name
 *
 * @param      $a
 * @param      $b
 *
 * @return int
 * @internal param bool $order
 *
 */
function sortByName($a, $b)
{
    return strcmp($a["name_cmp"], $b["name_cmp"]);
}

/**
 * @param $a
 * @param $b
 */
function sortByNameDesc($a, $b)
{
    strcmp($b["name_cmp"], $a["name_cmp"]);
}

/**
 * Function to sort by ID
 *
 * @param      $a
 * @param      $b
 *
 * @return mixed
 * @internal param bool $order
 *
 */
function sortById($a, $b)
{
    return $a['id'] - $b['id'];
}

/**
 * @param $a
 * @param $b
 *
 * @return mixed
 */
function sortByIdDesc($a, $b)
{
    return $b['id'] - $a['id'];
}

/**
 * Function to sort by rating
 *
 * @param      $a
 * @param      $b
 *
 * @return int
 * @internal param bool $order
 *
 */
function sortByRating($a, $b)
{
    return strcmp($a["rating"], $b["rating"]);
}

/**
 * @param $a
 * @param $b
 *
 * @return int
 */
function sortByRatingDesc($a, $b)
{
    return strcmp($b['rating'], $a['rating']);
}

/**
 * @return array
 */
function options_restaurants()
{
    return [
        [
            'name'        => 'limit',
            'mandatory'   => false,
            'description' => 'Omeji število poslanih restavracij v aplikacijo',
            'default'     => null
        ],
        [
            'name'        => 'lat',
            'mandatory'   => false,
            'description' => 'Zemljepisna širina uporabnika',
            'default'     => 0.0
        ],
        [
            'name'        => 'lng',
            'mandatory'   => false,
            'description' => 'Zemljepisna dolžina uporabnika',
            'default'     => 0.0
        ],
        [
            'name'        => 'order',
            'mandatory'   => false,
            'description' => 'Razvrsti restavracije po danem ključu',
            'default'     => 'distance_asc',
            'keys'        => [
                [
                    'name'        => 'distance_asc',
                    'description' => 'Razvrsti restavracije po oddaljenosti od uporabnika naraščujoče (od najbližje do najbolj oddaljene)'
                ],
                [
                    'name'        => 'distance_desc',
                    'description' => 'Razvrsti restavracije po oddaljenosti od uporabnika (od najbolj oddaljene do najbližje)'
                ],
                [
                    'name'        => 'name_asc',
                    'description' => 'Razvrsti restavracije po imenu (od A do Ž)'
                ],
                [
                    'name'        => 'name_desc',
                    'description' => 'Razvrsti restavracije po imenu (od Ž do A)'
                ],
                [
                    'name'        => 'rating_asc',
                    'description' => 'Razvrsti restavracije po povprečni skupni oceni naraščujoče'
                ],
                [
                    'name'        => 'rating_desc',
                    'description' => 'Razvrsti restavracije po povprečni skupni oceni padajoče'
                ],
                [
                    'name'        => 'id_asc',
                    'description' => 'Razvrsti restavracije po internem ID-ju baze naraščujoče'
                ],
                [
                    'name'        => 'id_desc',
                    'description' => 'Razvrsti restavracije po internem ID-ju baze padajoče'
                ]
            ]
        ]
    ];
}

/**
 * @return array
 */
function options_restaurant()
{
    return [
        [
            'name'        => 'lat',
            'mandatory'   => false,
            'description' => 'Zemljepisna širina uporabnika',
            'default'     => 0.0
        ],
        [
            'name'        => 'lng',
            'mandatory'   => false,
            'description' => 'Zemljepisna dolžina uporabnika',
            'default'     => 0.0
        ],
        [
            'name'        => 'logged',
            'default'     => 'false',
            'description' => 'V primeru da je (<samp>logged</samp> == <var>true</var>) so v rezulatu vključeni še klomentarji restavracije',
            'mandatory'   => false
        ]
    ];
}

/**
 * @return array
 */
function options_register()
{
    return [
        [ 'name' => 'email', 'mandatory' => true, 'description' => 'E-naslov uporabnika', 'default' => false ],
        [ 'name' => 'password', 'mandatory' => true, 'description' => 'Geslo uporabnika', 'default' => false ],
        [
            'name'        => 'password_verify',
            'mandatory'   => true,
            'description' => 'Ponovljeno geslo uporabnika',
            'default'     => false
        ]
    ];
}

/**
 * @return array
 */
function options_login()
{
    return [
        [ 'name' => 'email', 'mandatory' => true, 'description' => 'E-naslov uporabnika', 'default' => false ],
        [ 'name' => 'password', 'mandatory' => true, 'description' => 'Geslo uporabnika', 'default' => false ]
    ];
}

/**
 * @return array
 */
function options_social()
{
    return [
        [
            'name'        => 'provider',
            'mandatory'   => true,
            'description' => 'Ime socialnega omrežja preko katerega se je uporabnik prijavil',
            'default'     => false,
            'keys'        => [ [ 'name' => 'facebook' ], [ 'name' => 'twitter' ], [ 'name' => 'github' ] ]
        ],
        [
            'name'        => 'provider_id',
            'mandatory'   => true,
            'description' => 'ID uporabnika na tem socialnem omrežju',
            'default'     => false
        ],
        [
            'name'        => 'email',
            'mandatory'   => true,
            'description' => 'E-naslov uporabnika na izbranem socialnem omrežju.',
            'default'     => false
        ],
        [
            'name'        => 'name',
            'mandatory'   => false,
            'default'     => 'null',
            'description' => 'Ime in priimek uporabnika na socialnem omrežju'
        ]
    ];
}

/**
 * @return array
 */
function options_comment()
{
    return [
        [ 'name' => 'user', 'mandatory' => true, 'default' => null, 'description' => 'ID uporabnika' ],
        [
            'name'        => 'restaurant',
            'mandatory'   => true,
            'default'     => null,
            'description' => '<samp>Original ID</samp> restavracije'
        ],
        [ 'name' => 'comment', 'mandatory' => true, 'default' => null, 'description' => 'Vsebina komentarja' ]
    ];
}

/**
 * @return array
 */
function options_favourite()
{
    return [
        [ 'name' => 'user', 'mandatory' => true, 'default' => null, 'description' => 'ID uporabnika' ],
        [
            'name'        => 'restaurant',
            'mandatory'   => true,
            'default'     => null,
            'description' => '<samp>Original ID</samp> restavracije'
        ]
    ];
}

/**
 * @return array
 */
function options_offline()
{
    return [
        [
            'name'        => 'limit',
            'mandatory'   => false,
            'default'     => null,
            'description' => 'Omeji število poslanih restavracij v aplikacijo'
        ]
    ];
}

/**
 * @return array
 */
function options_rate()
{
    return [
        [ 'name' => 'user', 'mandatory' => true, 'default' => null, 'description' => 'ID uporabnika' ],
        [
            'name'        => 'restaurant',
            'mandatory'   => true,
            'default'     => null,
            'description' => '<samp>Original ID</samp> restavracije'
        ],
        [ 'name' => 'rate', 'mandatory' => true, 'default' => null, 'description' => 'Ocena od 1 do 5' ],
        [
            'name'        => 'type',
            'mandatory'   => true,
            'default'     => null,
            'description' => 'Tip ocene',
            'keys'        => [
                [ 'name' => 'quality', 'description' => 'Kvaliteta hrane' ],
                [ 'name' => 'clean', 'description' => 'Čistoča restavracije' ],
                [ 'name' => 'service', 'description' => 'Kvaliteta strežbe' ],
                [ 'name' => 'ambient', 'description' => 'Ambient v restavraciji' ]
            ]
        ]
    ];
}

/**
 * @return array
 */
function options_search()
{
    return [
        [ 'name' => 'search', 'mandatory' => true, 'default' => null, 'description' => 'Iskalni niz' ],
        [
            'name'        => 'user',
            'mandatory'   => false,
            'default'     => 'null',
            'description' => 'ID uporabnika ki je zahteval iskanje'
        ],
        [
            'name'        => 'order',
            'mandatory'   => false,
            'default'     => 'id_asc',
            'description' => 'Razvrstitev rezultatov',
            'keys'        => [
                [ 'name' => 'id_asc', 'description' => 'ID naraščujoče' ],
                [ 'name' => 'id_desc', 'description' => 'ID padajoče' ],
                [ 'name' => 'price_asc', 'description' => 'Cena naraščujoče' ],
                [ 'name' => 'price_desc', 'description' => 'Cena padajoče' ]
            ]
        ],
        [
            'name'        => 'lat',
            'mandatory'   => false,
            'description' => 'Zemljepisna širina uporabnika',
            'default'     => 0.0
        ],
        [
            'name'        => 'lng',
            'mandatory'   => false,
            'description' => 'Zemljepisna dolžina uporabnika',
            'default'     => 0.0
        ]
    ];
}

/**
 * @return array
 */
function result_restaurants()
{
    return [
        [ 'name' => 'id', 'description' => 'Interni ID v podatkovni bazi', 'type' => 'int' ],
        [
            'name'        => 'original_id',
            'description' => 'Hash, ki povezuje restavracijo z menijem, oceno, komentarjem, priljubljenim',
            'type'        => 'string'
        ],
        [ 'name' => 'name', 'description' => 'Ime restavracije', 'type' => 'string' ],
        [ 'name' => 'address', 'description' => 'Naslov restavracije', 'type' => 'string' ],
        [ 'name' => 'price', 'description' => 'Cena doplačila ob uporabi študentskega bona', 'type' => 'string' ],
        [ 'name' => 'city', 'description' => 'Mesto v katerem se nahaja restaracija', 'type' => 'string' ],
        [ 'name' => 'lat', 'description' => 'Zemljepisna širina restavracije', 'type' => 'double' ],
        [ 'name' => 'lng', 'description' => 'Zemljepisna dolžina restavracije', 'type' => 'double' ],
        [ 'name' => 'phones', 'description' => 'Vse telefonske številke restavracije', 'type' => 'array' ],
        [
            'name'        => 'distance',
            'description' => 'Razdalja med uporabnikom in restavracijo (zračna razdalja)',
            'type'        => 'string'
        ],
        [ 'name' => 'rating', 'description' => 'Povprečna ocena restavracije', 'type' => 'float' ],
        [
            'name'        => 'comments',
            'description' => 'Številko komentarjev restavracije',
            'type'        => 'integer',
            'new'         => true
        ],
        [
            'name'        => 'time_remaning',
            'description' => 'Zapisano koliko je še do odprtja/zaprtja restavracije',
            'type'        => 'string'
        ],
        [
            'name'        => 'opened',
            'description' => 'Stanje restavracije (odprta, zaprta, nimamo podatka)',
            'type'        => 'bool | null'
        ]
    ];
}

/**
 * @return array
 */
function result_restaurant()
{
    return [
        [ 'name' => 'id', 'description' => 'Interni ID v podatkovni bazi', 'type' => 'int' ],
        [
            'name'        => 'original_id',
            'description' => 'Hash, ki povezuje restavracijo z menijem, oceno, komentarjem, priljubljenim',
            'type'        => 'string'
        ],
        [ 'name' => 'name', 'description' => 'Ime restavracije', 'type' => 'string' ],
        [ 'name' => 'address', 'description' => 'Naslov restavracije', 'type' => 'string' ],
        [ 'name' => 'price', 'description' => 'Cena doplačila ob uporabi študentskega bona', 'type' => 'string' ],
        [ 'name' => 'city', 'description' => 'Mesto v katerem se nahaja restaracija', 'type' => 'string' ],
        [ 'name' => 'weekdays', 'description' => 'Odpiralni čas restavracije med tednom', 'type' => 'string' ],
        [ 'name' => 'saturdays', 'description' => 'Odpiralni čas restavracije ob sobotah', 'type' => 'string' ],
        [
            'name'        => 'sundays',
            'description' => 'Odpiralni čas restavracije ob nedeljah in praznikih',
            'type'        => 'string'
        ],
        [ 'name' => 'lat', 'description' => 'Zemljepisna širina restavracije', 'type' => 'double' ],
        [ 'name' => 'lng', 'description' => 'Zemljepisna dolžina restavracije', 'type' => 'double' ],
        [ 'name' => 'phones', 'description' => 'Vse telefonske številke restavracije', 'type' => 'array' ],
        [
            'name'        => 'distance',
            'description' => 'Razdalja med uporabnikom in restavracijo (zračna razdalja)',
            'type'        => 'string'
        ],
        [ 'name' => 'rating', 'description' => 'Povprečna ocena restavracije', 'type' => 'float' ],
        [
            'name'        => 'time_remaning',
            'description' => 'Zapisano koliko je še do odprtja/zaprtja restavracije',
            'type'        => 'string'
        ],
        [
            'name'        => 'opened',
            'description' => 'Stanje restavracije (odprta, zaprta, nimamo podatka)',
            'type'        => 'bool | null'
        ],
        [
            'name'        => 'menus',
            'description' => 'Vsebuje vse menije na voljo v restavraciji ali pa string \'Ni dnevnih menijev!\'',
            'type'        => 'array | string'
        ],
        [
            'name'        => 'ratings',
            'description' => 'Vsebuje polje vseh posameznih ocen za vsako izmed kategorij',
            'type'        => 'array'
        ],
        [
            'name'        => 'comments',
            'description' => 'Vsebuje polje vseh komentarjev restavracije (avtor, vsebina, datum). To polje je vključeno v rezultat le če je uporabnik prijavljen!',
            'type'        => 'array'
        ]
    ];
}

/**
 * @return array
 */
function result_register()
{
    return [
        [
            'name'        => 'response',
            'description' => 'Vsebuje rezutat ali je bila kacija izvedena uspešno ali ne',
            'type'        => 'bool'
        ],
        [ 'name' => 'message', 'description' => 'Vsebuje sporočilo ob izvšeni akciji', 'type' => 'string' ]
    ];
}

/**
 * @return array
 */
function result_login()
{
    return [
        [ 'name' => 'name', 'type' => 'string', 'description' => 'Ime in priimek uporabnika' ],
        [ 'name' => 'address', 'type' => 'string', 'description' => 'Naslov uporabnika, če ga je vnesel' ],
        [ 'name' => 'username', 'type' => 'string', 'description' => 'Uporabniško ime, če ga je vnesel' ],
        [ 'name' => 'email', 'type' => 'string', 'description' => 'E-naslov uporabnika' ],
        [
            'name'        => 'lat',
            'type'        => 'double',
            'description' => 'Zemljepisna širina uporabnika, če ima vnešen naslov'
        ],
        [
            'name'        => 'lng',
            'type'        => 'double',
            'description' => 'Zemljepisna dolžina uporabnika, če ima vnešen naslov'
        ]
    ];
}

/**
 * @return array
 */
function result_comment()
{
    return [
        [
            'name'        => 'response',
            'description' => 'Vsebuje rezutat ali je bila kacija izvedena uspešno ali ne',
            'type'        => 'bool'
        ],
        [ 'name' => 'message', 'description' => 'Vsebuje sporočilo ob izvšeni akciji', 'type' => 'string' ]
    ];
}

/**
 * @return array
 */
function result_rate()
{
    return [
        [
            'name'        => 'response',
            'description' => 'Vsebuje rezutat ali je bila kacija izvedena uspešno ali ne',
            'type'        => 'bool'
        ],
        [ 'name' => 'message', 'description' => 'Vsebuje sporočilo ob izvšeni akciji', 'type' => 'string' ]
    ];
}

/**
 * @return array
 */
function result_favourite()
{
    return [
        [
            'name'        => 'response',
            'description' => 'Vsebuje rezutat ali je bila kacija izvedena uspešno ali ne',
            'type'        => 'bool'
        ],
        [ 'name' => 'message', 'description' => 'Vsebuje sporočilo ob izvšeni akciji', 'type' => 'string' ],
        [
            'name'        => 'type',
            'type'        => 'bool',
            'description' => 'Če je <samp>true</samp>, potem je restavracijo dodal med priljubljene, drugače odstranil'
        ]
    ];
}

/**
 * @return array
 */
function result_social()
{
    return [
        [
            'name'        => 'response',
            'description' => 'Vsebuje rezutat ali je bila kacija izvedena uspešno ali ne',
            'type'        => 'bool'
        ],
        [ 'name' => 'message', 'description' => 'Vsebuje sporočilo ob izvšeni akciji', 'type' => 'string' ]
    ];
}

/**
 * @return array
 */
function result_offline()
{
    return [
        [ 'name' => 'id', 'description' => 'Interni ID v podatkovni bazi', 'type' => 'int' ],
        [
            'name'        => 'original_id',
            'description' => 'Hash, ki povezuje restavracijo z menijem, oceno, komentarjem, priljubljenim',
            'type'        => 'string'
        ],
        [ 'name' => 'name', 'description' => 'Ime restavracije', 'type' => 'string' ],
        [ 'name' => 'address', 'description' => 'Naslov restavracije', 'type' => 'string' ],
        [ 'name' => 'price', 'description' => 'Cena doplačila ob uporabi študentskega bona', 'type' => 'string' ],
        [ 'name' => 'city', 'description' => 'Mesto v katerem se nahaja restaracija', 'type' => 'string' ],
        [ 'name' => 'lat', 'description' => 'Zemljepisna širina restavracije', 'type' => 'double' ],
        [ 'name' => 'lng', 'description' => 'Zemljepisna dolžina restavracije', 'type' => 'double' ],
        [ 'name' => 'phones', 'description' => 'Vse telefonske številke restavracije', 'type' => 'array' ]
    ];
}

/**
 * @return array
 */
function result_search()
{
    return [
        [ 'name' => 'id', 'description' => 'Interni ID v podatkovni bazi', 'type' => 'int' ],
        [
            'name'        => 'original_id',
            'description' => 'Hash, ki povezuje restavracijo z menijem, oceno, komentarjem, priljubljenim',
            'type'        => 'string'
        ],
        [ 'name' => 'name', 'description' => 'Ime restavracije', 'type' => 'string' ],
        [ 'name' => 'address', 'description' => 'Naslov restavracije', 'type' => 'string' ],
        [ 'name' => 'price', 'description' => 'Cena doplačila ob uporabi študentskega bona', 'type' => 'string' ],
        [ 'name' => 'city', 'description' => 'Mesto v katerem se nahaja restaracija', 'type' => 'string' ],
        [ 'name' => 'lat', 'description' => 'Zemljepisna širina restavracije', 'type' => 'double' ],
        [ 'name' => 'lng', 'description' => 'Zemljepisna dolžina restavracije', 'type' => 'double' ],
        [ 'name' => 'phones', 'description' => 'Vse telefonske številke restavracije', 'type' => 'array' ],
        [
            'name'        => 'distance',
            'description' => 'Razdalja med uporabnikom in restavracijo (zračna razdalja)',
            'type'        => 'string'
        ],
        [ 'name' => 'rating', 'description' => 'Povprečna ocena restavracije', 'type' => 'float' ],
        [
            'name'        => 'comments',
            'type'        => 'integer',
            'new'         => true,
            'description' => 'Števlio komentarjev, ki jih ima restavracija.'
        ],
        [
            'name'        => 'time_remaning',
            'description' => 'Zapisano koliko je še do odprtja/zaprtja restavracije',
            'type'        => 'string'
        ],
        [
            'name'        => 'opened',
            'description' => 'Stanje restavracije (odprta, zaprta, nimamo podatka)',
            'type'        => 'bool | null'
        ]
    ];
}

function imageUrl($prefix, $suffix)
{
    return $prefix . env('DEFAULT_IMAGE_SIZE') . $suffix;
}

/**
 * Set's an array for permanent changes of restaurants
 *
 * @return array
 */
function getChanges()
{
    return [
        // 1 Vozi Miško MC Ljubljana; Restavracija 123 MC Ljubljana; Cantina Mexicana Lubljana; Terasa Mexicana
        11549 => [ "lat" => "46.087871", "lng" => "14.476805" ],
        12928 => [ "lat" => "46.087789", "lng" => "14.474981" ],
        11005 => [ "lat" => "46.022189", "lng" => "14.537040" ],
        12894 => [ "lat" => "46.088426", "lng" => "14.476360" ],
        // 2 Vozi Miško Ljubljana; Piazza del papa Ljubljana; Running Sushi & wok Ljubljana
        12901 => [ "lat" => "46.022956", "lng" => "14.536150" ],
        11284 => [ "lat" => "46.022017", "lng" => "14.535409" ],
        10982 => [ "lat" => "46.021511", "lng" => "14.535742" ],
        // 3 PE Oštarija Celje; Restavracija interspar Celje; Pomaranča Celje
        11278 => [ "lat" => "46.242080", "lng" => "15.276876" ],
        11430 => [ "lat" => "46.242026", "lng" => "15.279196" ],
        11378 => [ "lat" => "46.240994", "lng" => "15.277776" ],
        // 4 Pizzeria Al Capone Maribor; Restavracija Interspar Maribor Europark; Takos dos Maribor; Uno momento Pizza time Maribor; Restavracija McDonalds Europark Maribor
        11322 => [ "lat" => "46.553830", "lng" => "15.653198" ],
        11435 => [ "lat" => "46.554051", "lng" => "15.651320" ],
        12892 => [ "lat" => "46.554280", "lng" => "15.652339" ],
        11540 => [ "lat" => "46.552841", "lng" => "15.653970" ],
        11461 => [ "lat" => "46.553011", "lng" => "15.652565" ],
        // 5 Pizzeria da noi fast food; Restavracija 123 Maribor; Pizzeria in špagetarija Da noi fast food
        13023 => [ "lat" => "46.539647", "lng" => "15.640209" ],
        12926 => [ "lat" => "46.540377", "lng" => "15.642043" ],
        13027 => [ "lat" => "46.539875", "lng" => "15.641636" ],
        // 6 Mehiška restavracija Imperio - dostava BTC Ljubljana; Mehiška restavracija Imperio BTC Ljubljana; AL CAPONE BTC Ljubljana; Restavracija Interspar Citypark Ljubljana; Restavracija Burger King BTC; Tajska restavracija Bangkok Street BTC; Kitajska restavracija Han; Balkan Express; Oštarija pri Oljki
        11208 => [ "lat" => "46.069157", "lng" => "14.544800" ],
        11209 => [ "lat" => "46.069157", "lng" => "14.544618" ],
        12865 => [ "lat" => "46.068730", "lng" => "14.544772" ],
        11431 => [ "lat" => "46.068137", "lng" => "14.545175" ],
        11386 => [ "lat" => "46.067431", "lng" => "14.545588" ],
        11521 => [ "lat" => "46.066725", "lng" => "14.546087" ],
        13095 => [ "lat" => "46.066385", "lng" => "14.546884" ],
        12945 => [ "lat" => "46.066865", "lng" => "14.547220" ],
        12852 => [ "lat" => "46.067638", "lng" => "14.546548" ],
        // Restavracija 10-ka, Ekonomska fakulteta Ljubljana; Economist point; Greeny
        13111 => [ "lat" => "46.073855", "lng" => "14.517453" ],
        13105 => [ "lat" => "46.073877", "lng" => "14.516427" ],
        12818 => [ "lat" => "46.074337", "lng" => "14.516512" ],
        // NetCafe; Okrepčevalnica Badovinac; Bistro Šumi; Okrepčevalnica Šumi
        11224 => [ "lat" => "46.074077", "lng" => "14.514962" ],
        12837 => [ "lat" => "46.074174", "lng" => "14.514203" ],
        10995 => [ "lat" => "46.074211", "lng" => "14.513561" ],
        12846 => [ "lat" => "46.074678", "lng" => "14.514363" ],
        // Fast food pri Štuku - objekt pod Pohorjem
        12959 => [ "name" => "Fast food pri Štuku" ]
    ];
}

function cleanMenuString($menu)
{
    $string_mod = preg_replace('/ (ali|s|z|v|na|\*[0-9]+) /', " ", translate($menu));
    $string_mod = preg_replace('/([0-9]+\.|,|;|\.|\(|\)|\b\–\b|\b\-\b|\/|\:|\+|\?|\[|\])/', " ", $string_mod);
    $string_mod = preg_replace('/\s\s+/', ' ', $string_mod);

    return $string_mod;
}