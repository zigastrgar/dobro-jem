<?php

namespace App\Jobs;

use App\Restaurant;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FixRestaurantData extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $array = getChanges();
        foreach ( $array as $id => $changes ) {
            Restaurant::where('original_id', $id)->update($changes);
        }
    }
}
