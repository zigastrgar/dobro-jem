@extends('app')

@section('content')
    <div class="col-xs-12">
        <h1 class="page-header text-center">Prijava napake/pohvale ;)</h1>
        {!! Form::open(['url' => 'report', 'method' => 'POST']) !!}
        <div class='form-group col-xs-12 col-sm-12 col-md-4'>
            {!! Form::label('title', 'Naslov napake') !!} *
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
        <div class='form-group col-xs-12 col-sm-12 col-md-4'>
            {!! Form::label('email', 'Vaš e-naslov') !!}
            {!! Form::text('email', null, ['class' => 'form-control']) !!}
            <span class="help-block">Če oddate e-naslov, Vas obvestimo ob odpravitvi napake ;)</span>
        </div>
        <div class='form-group col-xs-12 col-sm-12 col-md-4'>
            {!! Form::label('type', 'Tip napake *') !!}
            <select name="type" class="form-control">
                @foreach($types as $type)
                    <option value="{{ $type->id }}">{{ $type->title }}</option>
                @endforeach
            </select>
        </div>
        <div class="clearfix"></div>
        <div class='form-group col-xs-12'>
            {!! Form::label('link', 'Povezava do strani z napako') !!}
            {!! Form::text('link', null, ['class' => 'form-control']) !!}
            <span class="help-block">Vsaka povezava nam bo pomagala pri hitrejši odpravi napake.</span>
        </div>
        <div class="form-group col-xs-12">
            {!! Form::label('content', 'Opis napake') !!}
            <textarea name="content" class="form-control col-xs-12" id="content" rows="5"></textarea>
        </div>
        <div class="col-xs-12">
            <button type="submit" class="btn btn-brand">Pošlji napako/pohvalo</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection