<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexKeywordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('index_keyword', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('index_id')->unsigned();
            $table->foreign('index_id')->references('id')->on('indexes')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('keyword_id')->unsigned();
            $table->foreign('keyword_id')->references('id')->on('keywords')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('index_keyword', function(Blueprint $table) {
            $table->dropForeign('index_keyword_index_id_foreign');
            $table->dropForeign('index_keyword_keyword_id_foreign');
        });

        Schema::drop('index_keyword');
    }
}
