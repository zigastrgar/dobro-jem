<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function(Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('link')->nullable();
            $table->text('comment')->nullable();
            $table->string('email')->nullable();
            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')->references('id')->on('report_types');
            $table->integer('user_id')->nullable();
            $table->string('ip', 20);
            $table->string('agent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function(Blueprint $table){
            $table->dropForeign('reports_type_id_foreign');
        });

        Schema::drop('reports');
    }
}
