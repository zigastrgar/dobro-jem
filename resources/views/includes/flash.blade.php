@if (Session::has('flash_notification.message'))
    <div class="col-xs-12 col-sm-8 col-sm-push-2 col-md-6 col-md-push-3">
        <div class="alert alert-{{ Session::get('flash_notification.level') }} alert-fix">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><span class="icon icon-times"></span></button>
            {!!  Session::get('flash_notification.message')  !!}
        </div>
    </div>
    <div class="clearfix"></div>
@endif