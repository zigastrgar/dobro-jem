<?php

namespace App\Http\Controllers;

use App\Feature;
use App\Report;
use App\ReportType;
use App\Restaurant;
use App\User;
use Auth;
use GeoIP;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use SEO;

class PagesController extends Controller
{

    /**
     * Homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurants = Restaurant::all()->random(2);

        return view("pages.index", compact('restaurants'));
    }

    /**
     * Nearest restaurants
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function nearest()
    {
        SEO::setTitle('Najbližje');

        $restaurants = Restaurant::all();

        return view("pages.nearest", compact('restaurants'));
    }

    public function report()
    {
        $types = ReportType::all();

        return view('pages.report', compact('types'));
    }

    public function postReport()
    {
        if ( Report::insert([
            'title'   => Request::get('title'),
            'comment' => Request::get('content'),
            'email'   => Request::get('email'),
            'link'    => Request::get('link'),
            'ip'      => $_SERVER['REMOTE_ADDR'],
            'agent'   => $_SERVER['HTTP_USER_AGENT'],
            'type_id' => Request::get('type'),
            'user_id' => Auth::id()
        ])
        ) {
            flash()->success('Napaka/predlog oddan uspešno!');

            return redirect('report');
        }
        flash()->error('Napaka/predlog oddan neuspešno!');

        return redirect('report');
    }

    /**
     * Search
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        SEO::setTitle('Iskanje');

        $max    = DB::table('restaurants')->max('value_of_charge');
        $min    = DB::table('restaurants')->min('value_of_charge');
        $prices = [
            'max' => ceil($max / 0.05) * 0.05,
            'min' => floor($min / 0.05) * 0.05,
            'mid' => round(( $max + $min ) / 2, 2)
        ];
        $prices = array_add($prices, 'low', ( round(( $prices['mid'] + $min ) / 2, 2) ));
        $prices = array_add($prices, 'high', ( round(( $prices['mid'] + $max ) / 2, 2) ));

        $features = Feature::all();

        $cities = Restaurant::groupBy('city')->get()->toArray();

        return view("pages.search", compact('prices', 'features', 'cities'));
    }

    /**
     * Login page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        SEO::setTitle('Prijava');

        return view("pages.login");
    }

    /**
     * Edit profile
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editProfile()
    {
        SEO::setTitle('Urejanje profila');

        return view('pages.edit_profile');
    }

    /**
     * Save profile
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function saveProfile()
    {
        $user = User::findOrFail(Auth::id());
        $geo  = [ ];
        if ( !empty( Request::get('address') ) )
            $geo = addressToGeoLocation(Request::get('address'));
        $user->update(array_merge(Request::all(), $geo));

        flash()->success('Vaš profil je bil <strong>uspešno</strong> spremenjen!');

        return redirect('editProfile');
    }

    public function searchBasic()
    {
        $query   = Request::get('search');
        $results = Restaurant::distance()->where(function($q) {
            $q->where('city', 'like', '%' . Request::get('search') . '%')
                ->orWhere('name', 'like', '%' . Request::get('search') . '%')
                ->orWhere('address', 'like', '%' . Request::get('search') . '%');
        })->ordering(Request::get('order'))->paginate(18);
        if ( count($results) == 0 ) {
            flash()->error("Iskanje žal <strong>ni</strong> obrodilo sadov!");
        }

        return view('pages.search_results', compact('results', 'query'));
    }

    public function searchAdvanced()
    {
        if ( Request::has('feature') ) {
            $feature = Request::get('feature');
        } else {
            $feature = [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ];
        }
        $query   = "";
        $results = Restaurant::distance()->where(function($q) use ($feature) {
            if ( Request::has('feature') ) {
                $q->where('city', 'like', '%' . Request::get('city') . '%')
                    ->where('name', 'like', '%' . Request::get('name') . '%')
                    ->where('address', 'like', '%' . Request::get('address') . '%')
                    ->whereBetween('value_of_charge', explode(";", Request::get('price')))
                    ->whereIn('original_id', DB::table('feature_restaurant')->select('restaurant_id')
                        ->whereIn('feature_id', $feature));
            } else {
                $q->where('city', 'like', '%' . Request::get('city') . '%')
                    ->where('name', 'like', '%' . Request::get('name') . '%')
                    ->where('address', 'like', '%' . Request::get('address') . '%')
                    ->whereBetween('value_of_charge', explode(";", Request::get('price')));
            }
        })->ordering(Request::get('order'))->paginate(18);

        return view('pages.search_results', compact('results', 'query'));
    }

    public function cookies()
    {
        SEO::setTitle('Piškoti');

        return view('pages.cookies');
    }

    public function terms()
    {
        SEO::setTitle('Pogoji uporabe');

        return view('pages.terms');
    }

    public function changelog()
    {
        SEO::setTitle('Dnevnik sprememb');

        return view('pages.changelog');
    }

    public function documentation()
    {
        SEO::setTitle('Dokumentacija API v2 Dobro jem');

        return view('pages.api.documentation');
    }

    public function lucky()
    {
        SEO::setTitle('Naključna restavracija ;)');

        $distance = ( Request::has('meters') ) ? Request::input('meters') : 2500;

        $restaurant = Restaurant::distance()->havingRaw('distance <= ' . $distance)->orderByRaw('RAND()')->first();

        return view('restaurants.lucky', compact('restaurant', 'distance'));
    }
}
