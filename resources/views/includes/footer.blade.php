<footer class="footer col-xs-12 center-block text-center">
    <div class="social-icons">
        <a class="icons" href="https://www.facebook.com/dobrojem.si" target="_blank"><span
                    class="icon icon-facebook font-2x"></span></a>
        <a class="icons" href="https://twitter.com/dobrojem" target="_blank"><span
                    class="icon icon-twitter font-2x"></span></a>
        <a class="icons" href="mailto:info@dobro-jem.si"><span class="icon icon-envelope-o font-2x"></span></a>
    </div>
    <div class="markets">
        <a target="_blank" href="https://play.google.com/store/apps/details?id=com.applications.primoz.dobrojemv3&hl=en"><img height="40px" alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" /></a>
    </div>
    <a href="/terms">Pogoji uporabe</a>&nbsp;&nbsp;•&nbsp;&nbsp;<a href="/cookies">Piškoti</a>&nbsp;&nbsp;•&nbsp;&nbsp;<a href="/report">Prijava napake</a>&nbsp;&nbsp;•&nbsp;&nbsp;<a
            href="/changelog">v 1.1.0</a>&nbsp;&nbsp;•&nbsp;&nbsp;<a href="/api/v2/documentation">API</a>&nbsp;&nbsp;•&nbsp;&nbsp;Dobro jem & <a href="http://webly.si" target="_blank">Webly</a> © 2016
</footer>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "url": "http://dobro-jem.si/",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "http://dobro-jem.si/search?search={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68403057-1', 'auto');
    ga('send', 'pageview');

</script>