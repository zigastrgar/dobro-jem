<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $table = "keywords";

    protected $fillable = [ 'content', 'created_at', 'updated_at' ];

    public function indexes()
    {
        return $this->belongsToMany('App\Index', 'index_keyword', 'keyword_id', 'index_id');
    }
}
