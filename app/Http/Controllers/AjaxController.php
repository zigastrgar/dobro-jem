<?php

namespace App\Http\Controllers;

use App\Favourite;
use App\Restaurant;
use App\User;
use App\Rating;
use Illuminate\Support\Facades\Auth;
use Request;
use App\Http\Controllers\Auth\AuthController as AController;

class AjaxController extends Controller
{
    public function checkEmail()
    {
        $user = User::where('email', Request::get('email'))->first();
        if ( $user !== null ) {
            return ( $user->provider != null ) ? $this->social() : $this->login();
        }

        return $this->register();
    }

    public function doAction(AController $authController)
    {
        $user   = User::where('email', Request::get('email'))->first();
        $status = $authController->isConsideredAdmin(Request::get('email'));
        if ( $user === null ) {
            $user = User::create([
                'email'      => Request::get('email'),
                'password'   => bcrypt(Request::get('password')),
                'first_time' => true,
                'status'     => $status
            ]);
        } else {
            if ( Auth::attempt([ 'email' => Request::get('email'), 'password' => Request::get('password') ]) ) {
                $user = Auth::user();
            }
            if ( $user === null ) {
                return "error|Napačen e-naslov ali geslo!";
            }
            $result = "success|true";
            if ( $user->first_time ) {
                $user->first_time = 0;
                $user->save();
                $result = "success|first_time";
            }
        }
        Auth::login($user, true);

        return $result;
    }


    public function rateRestaurant()
    { // TODO CHECK FOR DATA!!
        $restaurant = Request::get('restaurant');
        $user       = Auth::id();
        $type       = getRateTypeId(Request::get('type'));
        $rate       = Request::get('value');
        if ( !didRateRestaurant($user, $restaurant, Request::get('type')) ) {
            $rating   = Rating::create([
                'restaurant_id'  => $restaurant,
                'user_id'        => $user,
                'rating_type_id' => $type,
                'rate'           => $rate
            ]);
            $rest     = $rating->restaurant;
            $specific = getRating($rest->ratings()->where('rating_type_id', $type)->lists('rate'));
            $overall  = getRating($rest->ratings()->lists('rate'));
            $data     = [
                showStars($specific),
                "<p style='margin-top: 5px;'>$overall</p><p>" . showStars($overall) . "</p>",
                $specific
            ];

            return json_encode($data);
        }
    }

    public function setLocationMap()
    {
        session([ 'lat' => Request::get('lat'), 'lng' => Request::get('lng') ]);

        return "success";
    }

    public function like()
    {
        $restaurant = Request::get('restaurant');
        $user       = Auth::id();
        if ( !is_null($user) && Restaurant::where('original_id', $restaurant)->exists() ) {
            if ( !Favourite::where('user_id', $user)->where('restaurant_id', $restaurant)->exists() ) {
                Favourite::create([ 'restaurant_id' => $restaurant, 'user_id' => $user ]);

                return 'success|add|Restavracija uspešno dodana na seznam priljubljenih!';
            } else {
                Favourite::where('restaurant_id', $restaurant)->where('user_id', $user)->delete();

                return 'success|remove|Restavracija uspešno odstranjena iz seznama priljubljenih!';
            }
        } else {
            return 'error|Takšne restavracije ni v bazi, ali pa niste prijavljeni!';
        }
    }


    private function login()
    {
        return "login&%<h2>Prijava</h2>|<br />
<div class='input-group'>
<input type='password' name='password1' class='form-control normal-line-height' placeholder='Ponovite geslo'/>
                        <span class='input-group-btn'>
        <button class='btn btn-lg btn-success' id='go' data-action='login' type='button'>Prijava <span
                    class='icon icon-check'></span></button>
      </span>
</div>";
    }

    private function register()
    {
        return "register&%<h2>Registracija</h2>|<br /><input type='password' name='password' class='form-control normal-line-height' placeholder='Geslo'/>
<br />
<div class='input-group'>
<input type='password' name='password1' class='form-control normal-line-height' placeholder='Ponovite geslo'/>
                        <span class='input-group-btn'>
        <button class='btn btn-lg btn-success' id='go' data-action='register' type='button'>Registracija <span
                    class='icon icon-check'></span></button>
      </span>
</div>";
    }

    private function social()
    {
        return "social&%<h3>Ta mail je bil uporabljen kot prijava z enim od socialnih medijev!</h3>";
    }
}
