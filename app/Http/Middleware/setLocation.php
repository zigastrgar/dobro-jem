<?php

namespace App\Http\Middleware;

use Closure;

class setLocation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle( $request, Closure $next )
    {
        $ip = ($_SERVER["REMOTE_ADDR"] == "::1" || $_SERVER['REMOTE_ADDR'] == '192.168.10.1') ? "89.142.3.184" : $_SERVER["REMOTE_ADDR"];
        if ( empty(session('lat')) || empty(session('lng')) ) {
            $location = ip2Geo($ip);
            session(['lat' => $location['lat'], 'lng' => $location['lng']]);
        }
        return $next($request);
    }
}
