<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searches', function( Blueprint $table ) {
            $table->increments('id');
            $table->text('query');
            $table->integer('user_id')->nullable();
            $table->integer('results')->nullable();
            $table->string('ip');
            $table->string('agent', 400);
            $table->enum('machine', ['web', 'android', 'windows', 'ios'])->default('web');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('searches');
    }
}
