<script>
    $(function () {
        $("#quality[class=starrr]").starrr({
            rating: {!! floor(getRating($restaurant->ratings()->where('rating_type_id', getRateTypeId("quality"))->lists('rate'))) !!}
        });
        $("#service[class=starrr]").starrr({
            rating: {!! floor(getRating($restaurant->ratings()->where('rating_type_id', getRateTypeId("service"))->lists('rate'))) !!}
        });
        $("#clean[class=starrr]").starrr({
            rating: {!! floor(getRating($restaurant->ratings()->where('rating_type_id', getRateTypeId("clean"))->lists('rate'))) !!}
        });
        $("#ambient[class=starrr]").starrr({
            rating: {!! floor(getRating($restaurant->ratings()->where('rating_type_id', getRateTypeId("ambient"))->lists('rate'))) !!}
        });
    });

    $(document).on('starrr:change', function (e, value) {
        $.ajax({
            url: '/rateRestaurant',
            type: 'POST',
            data: {type: e.target.id, value: value, restaurant: '{{$restaurant->original_id}}', _token: '{{ csrf_token() }}'},
            success: function(cb){
                cb = $.parseJSON(cb);
                $("#overall").html(cb[1]);
                $("#"+ e.target.id).addClass('font-1-3-x').html(cb[0]).attr('title', cb[2]).attr('data-original-title', cb[2]);
            }
        });
    });
</script>