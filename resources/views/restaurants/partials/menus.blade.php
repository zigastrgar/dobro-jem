@if(count($restaurant->menus->toArray() > 0) && !empty($restaurant->menus->toArray()))
    <?php
    $i = 1;
    ?>
    <div class="col-xs-12">
        @foreach($restaurant->menus->chunk(6) as $chunck)
            <div class="row">
                @foreach($chunck as $menu)
                    <div class="col-xs-12 col-md-6 col-lg-4 text-center menu">
                        <h3>{{ $i }}</h3>
                        <ul class="list-unstyled menus">
                            @foreach(explode("(|)", $menu["content"]) as $food)
                                <li>{{ $food }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <?php
                    $i++;
                    ?>
                @endforeach
            </div>
        @endforeach
    </div>
@else
    <h3 class="text-center">Ni dnevnih menijev!</h3>
@endif
<div class="clearfix"></div>