<?php

namespace App\Http\Controllers;


use Carbon\Carbon;
use Request;
use App\Http\Controllers\Controller;
use DB;
use App\Restaurant;
use App\Search;
use Auth;
use SEO;

class SearchController extends Controller
{
    public function basic()
    {
        $search      = Request::get('search');
        $search_hash = translate($search);
        $query       = $search;

        SEO::setTitle("Rezultati iskanja: " . $search_hash);

        $results = Restaurant::distance()->where(function($q) use ($search_hash) {
            $q->whereRaw("original_id IN (SELECT DISTINCT(restaurant_id) FROM relations WHERE index_id IN (SELECT id FROM indexes WHERE content LIKE '%$search_hash%')) OR original_id IN (SELECT DISTINCT(restaurant_id) FROM relations WHERE index_id IN (SELECT index_id FROM index_keyword WHERE keyword_id IN (SELECT id FROM keywords WHERE content LIKE '%$search_hash%')))");
        })->ordering(Request::get('order'))->paginate(15);

        Search::insert([
            'query'      => $search_hash,
            'results'    => $results->total(),
            'user_id'    => Auth::id(),
            'agent'      => $_SERVER["HTTP_USER_AGENT"],
            'ip'         => $_SERVER["REMOTE_ADDR"],
            'updated_at' => Carbon::now(),
            'created_at' => Carbon::now()
        ]);

        return view('pages.search_results', compact('results', 'query'));
    }
}