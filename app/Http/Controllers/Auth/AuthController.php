<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use Laravel\Socialite\Facades\Socialite;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/restaurant';
    protected $redirectTo   = '/restaurant';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     *
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => bcrypt($data['password'])
        ]);
    }

    /**
     * Redirect the user to the provider authentication page.
     *
     * @param string $provider
     *
     * @return Response
     */

    public function socialLogin($provider = "facebook")
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Function called after redirected from social auth
     *
     * @param string $provider
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getData($provider = "facebook")
    {
        try {
            $user = Socialite::driver($provider)->user();
        } catch ( Exception $e ) {
            return redirect('auth/' . $provider);
        }
        $authUser = $this->findOrCreateUser($user, $provider);

        Auth::login($authUser, true);

        if ( !$authUser->first_time )
            return redirect()->back();

        return redirect('/editProfile');
    }

    /**
     * Find's or creates user
     *
     * @param $userData
     * @param $provider
     *
     * @return static
     */
    public function findOrCreateUser($userData, $provider)
    {
        $authData = User::where('provider_id', $userData->id)->where('provider', $provider)->first();

        if ( $authData )
            return $this->updateProfile($authData, $userData);

        $status = $this->isConsideredAdmin($userData->email);

        return User::create([
            'name'        => $userData->name,
            'email'       => $userData->email,
            'provider_id' => $userData->id,
            'username'    => $userData->nickname,
            'provider'    => $provider,
            'status'      => $status,
            'first_time'  => true
        ]);
    }

    /**
     * Set user permissions
     *
     * @param $mail
     *
     * @return string
     */
    public function isConsideredAdmin($mail)
    {
        return $mail == "ziga_strgar@hotmail.com" || $mail == "beno.gru@gmail.com" || $mail == "ramboss1337@gmail.com" || $mail == "nik.strozak@outlook.com" || $mail == "primoz.pesjak@gmail.com" ? 'admin' : 'user';
    }

    /**
     * Updates user profile
     *
     * @param $auth
     * @param $user
     *
     * @return mixed
     */
    private function updateProfile($auth, $user)
    {
        if ( !empty( $user->name ) )
            $auth->name = $user->name;
        if ( !empty( $user->nickname ) )
            $auth->username = $user->nickname;
        if ( !empty( $user->email ) )
            $auth->email = $user->email;
        $auth->first_time = false;
        $auth->save();

        return $auth;
    }

    /**
     * Logout's the user and redirect back to previous page
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout()
    {
        Auth::logout();
        flash()->success('Odjava je bila <strong>uspešna</strong>!');

        return redirect()->back();
    }
}
