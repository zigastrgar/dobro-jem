<h2 class="page-header text-center">Komentarji ({{ count($restaurant->comments) }})</h2>
@if(Auth::check())
    <div class="col-xs-12 col-sm-12 col-md-8 col-md-push-2">
        @foreach($restaurant->commentsOrdered as $comment)
            <div class="page-header">
                <h3>
                    @if(strlen($comment->user->username) > 0)
                        {{ $comment->user->username }}
                    @elseif(strlen($comment->user->email) > 0)
                        {{ $comment->user->email }}
                    @else
                        {{ $comment->user->name }}
                    @endif
                    <small data-toggle="tooltip" data-html="true" data-placement="bottom" class="pull-right"
                           title="{{ $comment->created_at->format('H:i:s j M Y') }}">{{ $comment->created_at->diffForHumans() }}</small>
                </h3>
            </div>
            <p>{{ $comment->content }}</p>
        @endforeach
        <h3 class="page-header text-center">Dodaj komentar</h3>
        {!! Form::open(['action' => ['CommentsController@add', 'restaurant' => $restaurant->original_id]]) !!}
        <div class="form-group col-xs-12">
            {!! Form::textarea('content', null, ['class' => 'form-control', 'placeholder' => 'Tu dodate komentar :)', 'rows' => 3]) !!}
            <input type="submit" class="btn btn-brand top-margin" value="Dodaj komentar">
        </div>
        {!! Form::close() !!}
    </div>
@else
    <h3 class="text-center">Za ogled komentarjev se prosimo <a
                href="{{ action('PagesController@login') }}">prijavite!</a></h3>
@endif