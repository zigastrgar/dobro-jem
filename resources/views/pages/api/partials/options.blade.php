<?php
$options = call_user_func("options_" . $data['id']);
if ( function_exists("result_" . $data['id']) ) {
    $result = call_user_func("result_" . $data['id']);
} else {
    $result = array();
}
?>
@if(count($options) > 0)
    <h3 class="page-header">Možnosti
        <small>Vse možnosti lahko pošljete z GET ali POST metodo</small>
    </h3>
    <table class="table table-bordered table-striped table-responsive">
        <tr>
            <th>Možnost</th>
            <th>Privzeta vrednost</th>
            <th>Opis</th>
        </tr>
        @foreach($options as $option)
            <tr>
                <td>
                    <samp @if($option['mandatory']) class="text-danger" @endif>{{ $option['name'] }}</samp>
                    @if(isset($option['new']))
                        <span class="label label-success">NOVO</span>
                    @endif
                </td>
                <td>
                    <var>{{ $option['default'] }}</var>
                </td>
                <td>
                    {!! $option['description'] !!}
                    @if(isset($option['keys']))
                        <br/>
                        <br/>
                        <p>Možnosti:</p>
                        <ul class="list-unstyled">
                            @foreach($option['keys'] as $key)
                                <li class="list-item"><samp>{{ $key['name'] }}</samp>@if(isset($key['description']))
                                        - {{ $key['description'] }}@endif</li>
                            @endforeach
                        </ul>
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
@else
    <p class="h3">Trenutna verzija ne zahteva nobenih dodatnih možnosti!</p>
@endif

@if(count($result) > 0)
    <h3 class="page-header">Rezultat
        <small>{{ $data['name'] }}</small>
    </h3>
    <table class="table table-bordered table-striped table-responsive">
        <tr>
            <td colspan="2">
                {{ $data['id'] }} | array
            </td>
        </tr>
        <tr>
            <th>Ključ</th>
            <th>Opis</th>
        </tr>
        @foreach($result as $res)
            <tr>
                <td>
                    <var>{{ $res['name'] }}</var>
                    @if(isset($res['new']))
                        <span class="label label-success">NOVO</span>
                    @endif
                </td>
                <td><samp>{{ $res['type'] }}</samp><br/>{!! $res['description'] !!}</td>
            </tr>
        @endforeach
    </table>
@endif