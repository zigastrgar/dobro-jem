@extends('app')

@section('content')
    <div class="col-xs-12">
        <h1 class="page-header text-center">
            Komentarji strani Dobro jem
        </h1>
        <table class="table table-responsive table-bordered table-condensed">
            <tr>
                <th>Datum</th>
                <th>Vsebina</th>
                <th>Uporabnik</th>
                <th>Restavracija</th>
                <th>Akcije</th>
            </tr>
            @foreach($comments as $comment)
                <tr>
                    <td>{{ $comment->created_at->format('d.m.Y H:i:s') }}</td>
                    <td>{{ $comment->content }}</td>
                    <td>@if(strlen($comment->user->name) > 0)<a href="{{ action('Admin\AdminController@more', ['id' => $comment->user->id]) }}">{{$comment->user->name}}</a>@endif
                    <td><a href="{{url('/restaurants/'.$comment->restaurant->slug)}}"> {{$comment->restaurant->name}}</a></td>
                    <td><a href="{{ action('Admin\AdminController@deleteComment', ['id' => $comment->id]) }}"> <span class="icon icon-times"></span></a></td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

