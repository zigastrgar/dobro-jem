<?php

namespace App\Console\Commands;

use App\Restaurant;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckLocations extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:locations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'It launches check for every geo location missing in database and fixes the problem';

    protected $restaurant;

    /**
     * Create a new command instance.
     *
     * @param Restaurant $restaurant
     */
    public function __construct(Restaurant $restaurant)
    {
        parent::__construct();

        $this->restaurant = $restaurant;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rests = Restaurant::all();
        $this->info(Carbon::now());
        foreach ( $rests as $rest ) {
            if ( ( empty( $rest->lat ) || empty( $rest->lng ) ) || ( $rest->lat == 0 || $rest->lng == 0 ) ) {
                $this->line('Posodabljam: ' . $rest->slug);
                $loc       = addressToGeoLocation($rest->address . "," . $rest->city);
                $rest->lat = $loc['lat'];
                $rest->lng = $loc['lng'];
                $rest->save();
            }
        }
    }
}
