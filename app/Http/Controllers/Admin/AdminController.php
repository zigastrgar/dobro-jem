<?php

namespace App\Http\Controllers\Admin;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Image;
use App\Index;
use App\Keyword;
use App\Menu;
use App\Report;
use App\Restaurant;
use App\User;
use DB;
use Illuminate\Database\QueryException;
use Request;
use SEO;
use SEOMeta;

class AdminController extends Controller
{

    public function index()
    {
        return view('admin.dashboard.index');
    }

    public function comments()
    {
        $comments = Comment::all();

        return view('admin.dashboard.comments', compact('comments'));
    }

    public function reports()
    {
        $reports = Report::all();

        return view('admin.dashboard.reports', compact('reports'));
    }

    public function statistics()
    {
        return view('admin.dashboard.statistics');
    }

    public function users()
    {
        $users = User::all();

        return view('admin.dashboard.users', compact('users'));
    }

    public function showpage($id)
    {
        return view('admin/upload_images', compact('id'));
    }

    public function more($id)
    {
        $user = User::find($id);

        return view('admin.dashboard.more', compact('user'));
    }

    public function upload_image(Request $request, $id)
    {
        print_r($request->all());
        $files = $request->file('file');
        if ( !empty( $files ) ):
            foreach ( $files as $file ):
                $client_id = env("IMGURL_API_KEY");
                $data      = file_get_contents($file);
                $pvars     = [ 'image' => base64_encode($data) ];
                $timeout   = 30;
                $curl      = curl_init();
                curl_setopt($curl, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
                curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
                curl_setopt($curl, CURLOPT_HTTPHEADER, [ 'Authorization: Client-ID ' . $client_id ]);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $pvars);
                $out = curl_exec($curl);
                curl_close($curl);
                $pms2  = json_decode($out, true);
                $url2  = $pms2['data']['link'];
                $image = $url2;
                //Storage::put($file->getClientOriginalName()  ,file_get_contents($file));
            endforeach;
        endif;
        DB::insert('insert into images (restaurant_id, url, primary) values (?, ?, ?)', [ $id, $image, 0 ]);
    }

    public function editImages($slug)
    {
        SEO::setTitle('Urejanje slik');

        $restaurant = Restaurant::where('slug', $slug)->first();

        return view('restaurants.images', compact('restaurant'));
    }

    public function deleteImage()
    {
        DB::table('images')->where('id', Request::get('id'))->delete();
        if ( is_null(DB::table('images')->where('restaurant_id', Request::get('restaurant'))
            ->where('primary', true)) ) {
            $image          = DB::table('images')->where('restaurant_id', Request::get('restaurant'))->first();
            $image->primary = true;
            $image->save();
        }
    }

    public function deleteComment($id)
    {
        DB::table('comments')->where('id', $id)->delete();

        return redirect('/dashboard/comments');

    }

    public function saveImage()
    {
        $restaurant = Request::get('restaurant');
        $id         = Request::get('value');
        DB::table('images')->where('restaurant_id', $restaurant)->update([ 'primary' => 0 ]);
        DB::table('images')->where('id', $id)->update([ 'primary' => 1 ]);
    }

    public function indexes()
    {
        $indexes = DB::table('indexes')->pluck("content");

        $restaurants = Restaurant::withoutGlobalScope('distance')->get();
        foreach ( $restaurants as $restaurant ) {
            $name = translate($restaurant->name);
            $city = translate($restaurant->city);
            if ( !in_array($name, $indexes) ) {
                DB::insert('INSERT INTO indexes (content, created_at, updated_at) VALUES (?, NOW(), NOW())', [ $name ]);
                $indexes[] = $name;
            }
            if ( !in_array($city, $indexes) ) {
                DB::insert('INSERT INTO indexes (content, created_at, updated_at) VALUES (?, NOW(), NOW())', [ $city ]);
                $indexes[] = $city;
            }
        }

        $menus = Menu::all();
        foreach ( $menus as $menu ) {
            foreach ( explode("(|)", $menu->content) as $food ) {
                $food = translate($food);
                if ( !empty( $food ) ) {
                    if ( !in_array($food, $indexes) ) {
                        DB::insert('INSERT INTO indexes (content, created_at, updated_at) VALUES (?, NOW(), NOW())', [ $food ]);
                        $indexes[] = $food;
                    }
                }
            }
        }

        return redirect('/relations');
    }

    public function relations()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('relations')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        $restaurants = Restaurant::withoutGlobalScope('distance')->withoutGlobalScope('active')->get();

        foreach ( $restaurants as $restaurant ) {
            $name = translate($restaurant->name);
            $city = translate($restaurant->city);
            DB::insert('INSERT INTO relations (result_type, result_id, index_id, restaurant_id) VALUES (\'restaurant\', ?, (SELECT id FROM indexes WHERE content LIKE ? LIMIT 1), ?)', [
                $restaurant->original_id,
                '%' . $name . '%',
                $restaurant->original_id
            ]);
            DB::insert('INSERT INTO relations (result_type, result_id, index_id, restaurant_id) VALUES (\'restaurant\', ?, (SELECT id FROM indexes WHERE content = ? LIMIT 1), ?)', [
                $restaurant->original_id,
                $city,
                $restaurant->original_id
            ]);
        }

        $menus = Menu::all();
        foreach ( $menus as $menu ) {
            foreach ( explode("(|)", $menu->content) as $food ) {
                $food = cleanMenuString($food);
                if ( !empty( $food ) ) {
                    foreach ( explode(" ", $food) as $string ) {
                        if ( !empty( $string ) ) {
                            try {
                                DB::insert('INSERT INTO relations (result_type, result_id, index_id, restaurant_id) VALUES (\'menu\', ?, (SELECT id FROM indexes WHERE content = ?), ?)', [
                                    $menu->id,
                                    $string,
                                    $menu->restaurant_id
                                ]);
                            } catch ( QueryException $ex ) {

                            }
                        }
                    }
                }
            }
        }

        return redirect("/");
    }

    public function keywords()
    {
        SEO::setTitle('Ključne besede');

        $searches =
            DB::table('searches')->select(DB::raw('*, COUNT(*) as number, MAX(results) AS maximum'))->groupBy('query')
                ->orderBy('number', 'DESC')->get();
        $keywords = Keyword::all();

        return view('admin.dashboard.keyword', compact('searches', 'keywords'));
    }

    public function addKeyword()
    {
        $keyword = Keyword::firstOrCreate([ 'content' => translate(Request::get('keyword')) ]);
        $in      = translate(Request::get('index'));
        $index   = Index::whereRaw("content LIKE '%$in%'")->orderByRaw('LENGTH(content) ASC')->first();
        if ( is_null($index) ) {
            flash()->error('Ta index: <strong>' . Request::get('index') . '</strong> ne obstaja!');
        } else {
            DB::insert('INSERT INTO index_keyword (index_id, keyword_id) VALUES (?, ?)', [ $index->id, $keyword->id ]);
            flash()->success("Ključna beseda: <strong>" . Request::get('keyword') . "</strong> uspešno povezana z <strong>" . Request::get('index') . "</strong>");
        }

        return redirect("/dashboard/keywords");
    }

    public function images()
    {
        $client = env('FOURSQUARE_CLIENT');
        $secret = env('FOURSQUARE_SECRET');

        $restaurants = Restaurant::all();

        foreach ( $restaurants as $restaurant ) {
            if ( !Image::where('restaurant_id', $restaurant->original_id)->exists() ) {
                $name = smartName(clear(translate(str_replace(" - dostava", "", strtolower($restaurant->name)))));
                $data =
                    json_decode(file_get_contents("https://api.foursquare.com/v2/venues/search?intent=checkin&radius=500&ll=" . $restaurant->lat . "," . $restaurant->lng . "&client_id=" . $client . "&client_secret=" . $secret . "&v=" . date('Ymd') . "&query=" . $name), true);
                if ( count($data['response']['venues']) > 0 ) {
                    $i = 0;
                    $d = $data['response']['venues'][0]['location']['distance'];
                    $c = 0;
                    foreach ( $data['response']['venues'] as $response ) {
                        if ( $response['location']['distance'] < $d ) {
                            $d = $response['location']['distance'];
                            $i = $c;
                        }
                        $c++;
                    }

                    $id = $data['response']['venues'][$i]['id'];

                    $data =
                        json_decode(file_get_contents("https://api.foursquare.com/v2/venues/$id/photos?client_id=$client&client_secret=$secret&v=" . date("Ymd")), true);

                    $prim = true;

                    foreach ( $data['response']['photos']['items'] as $item ) {
                        Image::create([
                            'restaurant_id' => $restaurant->original_id,
                            'prefix'        => $item['prefix'],
                            'suffix'        => $item['suffix'],
                            'primary'       => $prim,
                            'foursquare_id' => $id
                        ]);
                        $prim = false;
                    }
                }
            }
        }
    }

}
