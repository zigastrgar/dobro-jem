<?php

return [
    'meta'      => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'       => "Dobro jem", // set false to total remove
            'description' => 'Spletna stran s pregledom ponudnikov študentske prehrane, njihovih menijev in objektivne ocene ter komentarji študentov.', // set false to total remove
            'separator'   => ' - ',
            'keywords'    => ['preharana', 'študentska', 'študentska prehrana', 'študentje', 'boni', 'malica', 'obrok', 'restavracija', 'Ljubljana', 'Maribor', 'Celje', 'študetnski boni', 'poceni', 'dobro', 'dobro jem', 'študenti'],
        ],

        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => "BHyzSB_Mpzao9TZgPCuzM9j33QHms2k_2BXp7g_uD9o",
            'bing'      => "1C12A200AD667919270DD4C58939742C",
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => 'Dobro jem', // set false to total remove
            'description' => 'Spletna stran s pregledom ponudnikov študentske prehrane, njihovih menijev in objektivne ocene ter komentarji študentov.', // set false to total remove
            'url'         => 'http://dobro-jem.si',
            'type'        => false,
            'site_name'   => 'Dobro jem - Preveri ponudnike študentske prehrane',
            'images'      => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
          //'card'        => 'summary',
          //'site'        => '@LuizVinicius73',
        ],
    ],
];
