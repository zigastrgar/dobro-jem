<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    protected $table = "api_keys";

    protected $fillable = [ 'key', 'owner', 'created_at', 'updated_at', 'type', 'level', 'valid_until' ];

    public function logs()
    {
        return $this->hasMany('App\Log', 'key', 'key');
    }
}
