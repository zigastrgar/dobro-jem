<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Index extends Model
{
    protected $table = "indexes";

    protected $fillable = [ 'content' ];

    public function keywords()
    {
        return $this->belongsToMany('App\Keyword', 'index_keyword', 'index_id', 'keyword_id');
    }

    public function relations()
    {
        return $this->hasMany('App\Relation');
    }
}
