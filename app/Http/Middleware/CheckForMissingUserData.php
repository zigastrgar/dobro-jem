<?php

namespace App\Http\Middleware;

use Closure;

class CheckForMissingUserData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if($user && $request->getRequestUri() == "/missingData"){
            if(is_null($user->address) || is_null($user->lat) || is_null($user->lng)){
                return redirect('missingData');
            }
        }

        return $next($request);
    }
}
