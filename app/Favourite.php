<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
    protected $table = "favourites";

    protected $fillable = [ 'user_id', 'restaurant_id', 'created_at', 'updated_at' ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant', 'restaurant_id', 'original_id');
    }
}
