<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('features')->insert(['name' => 'Pizze', 'picture' => 'pizza', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('features')->insert(['name' => 'Vegi meni', 'picture' => 'food', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('features')->insert(['name' => 'Študentske ugodnosti', 'picture' => 'happy-smiley-very', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('features')->insert(['name' => 'Solate', 'picture' => 'ios-nutrition', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('features')->insert(['name' => 'Kosila', 'picture' => 'chef-food-restaurant-streamline', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('features')->insert(['name' => 'Dostava na dom', 'picture' => 'transportation-car', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('features')->insert(['name' => 'Odprto med vikendi', 'picture' => 'calendar-1', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('features')->insert(['name' => 'Dostopno invalidom', 'picture' => 'disability', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
        DB::table('features')->insert(['name' => 'Hitra hrana', 'picture' => 'fast-food', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
    }
}
