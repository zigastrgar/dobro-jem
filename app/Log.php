<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = "api_logs";

    protected $fillable = [ 'key', 'page', 'additional', 'created_at', 'updated_at' ];

    public function api()
    {
        return $this->hasOne('App\Api', 'key', 'key');
    }
}
