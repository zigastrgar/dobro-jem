<!doctype html>
<html lang="sl">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta property="fb:app_id" content="899428896779258" />
    {!! SEO::generate() !!}
    <meta name="robots" content="index, follow">
    @yield('headers')
    @include('includes.favicon')
    <link rel="stylesheet" href="{{ elixir('assets/css/app.css') }}" type="text/css">
</head>
<body>
@include('includes.menu')
<div class="container-fluid">
    <div class="row">
        @include('includes.flash')
        @yield('content')
    </div>
</div>
@include('includes.footer')

<script src="/assets/js/jquery.js"></script>
<script src="/assets/js/bootstrap.js"></script>
@yield('scripts')
</body>
</html>
