<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\CheckLocations::class,
        \App\Console\Commands\Slug::class,
        \App\Console\Commands\RestaurantImport::class,
        \App\Console\Commands\ClearMenus::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('check:locations')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('check:slugs')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('import:restaurants')->dailyAt('08:00')->thenPing("http://dobro-jem.si/relations");
        $schedule->command('import:restaurants')->dailyAt('09:00')->thenPing("http://dobro-jem.si/relations");
        $schedule->command('import:restaurants')->dailyAt('10:00')->thenPing("http://dobro-jem.si/relations");
        $schedule->command('import:restaurants')->dailyAt('11:00')->thenPing("http://dobro-jem.si/relations");
        $schedule->command('clear:menus')->dailyAt('08:10');
    }
}
