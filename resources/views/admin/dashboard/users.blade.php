@extends('app')

@section('content')
    <div class="col-xs-12">
        <h1 class="page-header text-center">
            Uporabniki strani Dobro jem
        </h1>
        <table class="table table-responsive table-bordered table-condensed">
            <tr>
                <th>ID</th>
                <th>Ime in priimek</th>
                <th>Uporabniško ime</th>
                <th>Komentarji</th>
                <th>Ocene</th>
                <th>Priljubjeno</th>
                <th>Naslov</th>
                <th>E-naslov</th>
                <th>Način prijave</th>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td><a href="{{ action('Admin\AdminController@more', ['id' => $user->id]) }}">{{ $user->id }}</a></td>
                    <td>{{ $user->name }} @if($user->status == "admin") <span class="label label-danger">Admin</span> @endif</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ count($user->comments->toArray()) }}</td>
                    <td>{{ count($user->ratings->toArray()) }}</td>
                    <td>{{ count($user->likes->toArray()) }}</td>
                    <td>{{ $user->address }}</td>
                    <td>@if(strlen($user->email) > 0)<a href="mailto:{{ $user->email }}">{{ $user->email}}</a>@endif
                    </td>
                    <td>@if(strlen($user->provider) > 0) {{ $user->provider }} @else Dobro jem @endif</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection