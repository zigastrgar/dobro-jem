<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Search extends Model
{
    protected $table = "searches";

    protected $fillable = [ 'query', 'ip', 'user_id', 'agent', 'results', 'created_at', 'updated_at', 'machine' ];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
