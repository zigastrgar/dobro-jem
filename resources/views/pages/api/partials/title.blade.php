<h2 id="{{ $data['id'] }}" class="page-header">
    {{ $data['name'] }}
    <small>
        <a target="_blank" href="{{ url('api/v2/'.$data['id']) }}">{{ url('api/v2/'.$data['id']) }}@if(isset($data['slug'])) /{{ $data['slug'] }} @endif</a>
    </small>
</h2>
@include('pages.api.partials.options')