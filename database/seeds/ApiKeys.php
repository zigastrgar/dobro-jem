<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ApiKeys extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('api_keys')->insert([ 'owner'       => 'ziga_strgar@hotmail.com',
                                        'key'         => 'AN9uVLUA16IWa36YfLzvvUm2NelW04B269HstkvW',
                                        'level'       => 4,
                                        'type'        => 'web',
                                        'valid_until' => Carbon::now()->addYears(5),
                                        'created_at'  => Carbon::now(),
                                        'updated_at'  => Carbon::now() ]);
        DB::table('api_keys')->insert([ 'owner'       => 'nik.strozak@outlook.com',
                                        'key'         => 'JQouaWjb4QKaRu1owDZu7vr1Deey9rYW69BRQzFh',
                                        'level'       => 4,
                                        'type'        => 'windows',
                                        'valid_until' => Carbon::now()->addYears(5),
                                        'created_at'  => Carbon::now(),
                                        'updated_at'  => Carbon::now() ]);
        DB::table('api_keys')->insert([ 'owner'       => 'primoz.pesjak@gmail.com',
                                        'key'         => 'WaPykaSWlgigi1mRinm4o09ZQ0vHlPlldGQ8An9w',
                                        'level'       => 4,
                                        'type'        => 'android',
                                        'valid_until' => Carbon::now()->addYears(5),
                                        'created_at'  => Carbon::now(),
                                        'updated_at'  => Carbon::now() ]);
        DB::table('api_keys')->insert([ 'owner'       => 'ziga_strgar@hotmail.com',
                                        'key'         => 'wmbh3EvV2ftSNyYFTfSlh402Tdo0XtMvUN8A0n1L',
                                        'level'       => 4,
                                        'type'        => 'ios',
                                        'valid_until' => Carbon::now()->addYears(5),
                                        'created_at'  => Carbon::now(),
                                        'updated_at'  => Carbon::now() ]);
    }
}
