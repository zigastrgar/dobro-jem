<div id="menu">
    <nav>
        <ul>
            <li><a href="{{ url('/') }}">Domov</a></li>
            <li><a href="{{ url('/restaurants') }}">Restavracije</a></li>
            <li><a href="{{ url('/nearest') }}">Najbližje</a></li>
            <li><a href="{{ url('/search_page') }}">Iskanje</a></li>
            @if(Auth::check())
                <li><a href="{{ url('/logout') }}">Odjava</a></li>
            @else
                <li><a href="{{ url('/login') }}">Prijava</a></li>
                <li><a href="{{ url('/login') }}">Registracija</a></li>
            @endif
        </ul>
    </nav>
</div>