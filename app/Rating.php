<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

    protected $table = "ratings";

    protected $fillable = [ 'rate', 'user_id', 'restaurant_id', 'rating_type_id', 'created_at', 'updated_at' ];

    /**
     * Returns all the users which rated with given rate
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Returns all the restaurants which belongs to given rate
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant', 'restaurant_id', 'original_id');
    }

    public function type()
    {
        return $this->hasOne('App\RatingType', 'id', 'rating_type_id');
    }
}
