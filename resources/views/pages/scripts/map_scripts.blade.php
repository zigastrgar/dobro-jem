<script async defer
        src="https://maps.googleapis.com/maps/api/js?callback=initGeoLocation"></script>
<script type="text/javascript">
    var map;
    var lat;
    var lng;
    function initMap() {
        map = new google.maps.Map(document.getElementById('nearest-map'), {
            center: {lat: lat, lng: lng},
            zoom: 15,
            disableDefaultUI: false,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var home = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map,
            icon: "../assets/img/blue.png"
        });

        var inf = new google.maps.InfoWindow({
            content: 'Tu sem jaz'
        });

        google.maps.event.addListener(home, 'click', function () {
            inf.open(map, home);
        });

        var marker, i, markers = [];
        var locations = [{!! restaurantsToJSArray($restaurants) !!}];

        for (i in locations) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][2], locations[i][3]),
                map: map,
                icon: locations[i][5]
            });

            var infowindow = new google.maps.InfoWindow({
                content: ''
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent("<a class='body-font' href='{{ url('/restaurants') }}/" + locations[i][4] + "'><p>" + locations[i][0] + "</p></a><p class='body-font'>" + locations[i][1] + "</p><p class='body-font'>Doplačilo: <strong>" + locations[i][6] + "</strong></p>");
                    infowindow.open(map, marker);
                }
            })(marker, i));
            markers.push(marker);
        }
    }

    function initGeoLocation() {
        if (navigator && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(successCall, errorCall);
        } else {
            console.log("Geopozicija ni mogoča!");
            initMap();
        }
    }

    function successCall(position) {
        lat = position.coords.latitude;
        lng = position.coords.longitude;
        $.ajax({
            url: '/setLocationMap',
            type: 'POST',
            data: {lat: lat, lng: lng, _token: '{!! csrf_token() !!}'},
            success: function () {
            }
        });
        initMap();
    }

    function errorCall() {
        @if(Auth::check() && !is_null(Auth::user()->lat) && !is_null(Auth::user()->lng))
                lat = {{ Auth::user()->lat }};
        lng = {{ Auth::user()->lng }};
        @else
                lat = {{ session('lat') }};
        lng = {{ session('lng') }};
        @endif
        initMap();
    }
</script>
<script>
    $(document).ready(function () {
        setInterval(function () {
            var height = $(document).height();
            if ((height - 365) < 300) {
                height = 565;
            }
            $("#nearest-map").css({'height': height - 365 + 'px'});
        }, 100);
    });
</script>