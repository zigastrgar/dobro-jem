<?php

use Illuminate\Support\Facades\Artisan;

Route::group([ 'middleware' => [ 'web' ] ], function() {
    // PAGES

    Route::get('/', 'PagesController@index');
    Route::get('nearest', 'PagesController@nearest');
    Route::get('search_page', 'PagesController@search');
    Route::get('cookies', 'PagesController@cookies');
    Route::get('terms', 'PagesController@terms');
    Route::get('changelog', 'PagesController@changelog');
    Route::get('report', 'PagesController@report');
    Route::post('report', 'PagesController@postReport');
    Route::get('lucky', 'PagesController@lucky');

    Route::resource('restaurants', 'RestaurantsController');

    // OTHER

    //Route::get('searchBasic/{params?}', 'PagesController@searchBasic');
    Route::get('searchAdvanced', 'PagesController@searchAdvanced');
    Route::post('checkEmail', 'AjaxController@checkEmail');
    Route::post('loginOrRegister', 'AjaxController@doAction');
    Route::get('search', 'SearchController@basic');
    Route::post('setLocationMap', 'AjaxController@setLocationMap');
    Route::get('login', 'PagesController@login')->middleware('guest');
    Route::get('auth/{provider}', 'Auth\AuthController@socialLogin');
    Route::get('auth/{provider}/callback', 'Auth\AuthController@getData');

    Route::get('api/v2/documentation', 'PagesController@documentation');
    Route::get('docs', 'PagesController@docs');

    // LOGGED

    Route::get('editProfile', 'PagesController@editProfile')->middleware('auth');
    Route::post('editProfile', 'PagesController@saveProfile')->middleware('auth');
    Route::post('comment/{restaurant}', 'CommentsController@add')->middleware('auth');
    Route::post('rateRestaurant', 'AjaxController@rateRestaurant')->middleware('auth');
    Route::post('like', 'AjaxController@like')->middleware('auth');
    Route::get('logout', 'Auth\AuthController@getLogout')->middleware('auth');
});

Route::group([ 'middleware' => [ 'web', 'admin' ] ], function() {
    Route::get('admin/{id?}', 'Admin\AdminController@showpage');

    Route::get('dashboard', 'Admin\AdminController@index');
    Route::get('dashboard/users', 'Admin\AdminController@users');
    Route::get('dashboard/statistics', 'Admin\AdminController@statistics');
    Route::get('dashboard/comments', 'Admin\AdminController@comments');
    Route::get('dashboard/reports', 'Admin\AdminController@reports');
    Route::get('dashboard/users/{id}', 'Admin\AdminController@more');
    Route::get('dashboard/keywords', 'Admin\AdminController@keywords');
    Route::post('keywords', 'Admin\AdminController@addKeyword');

    Route::get('restaurant_images', 'Admin\AdminController@images');
    Route::get('editImages/{slug}', 'Admin\AdminController@editImages');
    Route::post('editImages', 'Admin\AdminController@saveImage');
    Route::post('deleteImage', 'Admin\AdminController@deleteImage');
    Route::get('deleteComment/{id?}', 'Admin\AdminController@deleteComment');

    Route::post('admin/upload_image/{id}', 'Admin\AdminController@upload_image');
});

Route::group([ 'middleware' => [ 'api_token' ], 'prefix' => 'api/v2' ], function() {
    Route::match([ 'get', 'post' ], 'restaurants', 'API\RestaurantsApiController@index')->middleware('api_level:1');
    Route::match([ 'get', 'post' ], 'restaurants_mini', 'API\RestaurantsApiController@miniRestaurants')
        ->middleware('api_level:1');
    Route::match([ 'get', 'post' ], 'restaurant/{id}', 'API\RestaurantsApiController@show')->middleware('api_level:1');
    Route::match([ 'get', 'post' ], 'restaurant/{id}/menus', 'API\RestaurantsApiController@menus')
        ->middleware('api_level:1');
    Route::match([ 'get', 'post' ], 'restaurant/{id}/ratings', 'API\RestaurantsApiController@ratings')
        ->middleware('api_level:1');
    Route::match([ 'get', 'post' ], 'restaurant/{id}/comments', 'API\RestaurantsApiController@comments')
        ->middleware('api_level:1');
    Route::match([ 'get', 'post' ], 'offline', 'API\RestaurantsApiController@offline')->middleware('api_level:1');
    Route::match([ 'get', 'post' ], 'search', 'API\SearchApiController@search')->middleware('api_level:1');
    Route::match([ 'get', 'post' ], 'register', 'API\UsersApiController@register')->middleware('api_level:2');
    Route::match([ 'get', 'post' ], 'login', 'API\UsersApiController@login')->middleware('api_level:2');
    Route::match([ 'get', 'post' ], 'rate', 'API\RateApiController@rate')->middleware('api_level:3');
    Route::match([ 'get', 'post' ], 'like', 'API\LikeApiController@like')->middleware('api_level:3');
    Route::match([ 'get', 'post' ], 'social', 'API\UsersApiController@social')->middleware('api_level:3');
    Route::match([ 'get', 'post' ], 'comment', 'API\CommentsApiController@comment')->middleware('api_level:3');
});

Route::get('generateToken', function() {
    echo str_random(40);
});

Route::get('create_indexes', 'Admin\AdminController@indexes');
Route::get('relations', 'Admin\AdminController@relations');

Route::get('import_scripts', function() {
    Artisan::call("import:restaurants");
});