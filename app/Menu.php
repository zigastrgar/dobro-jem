<?php

namespace App;

use App\Index;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;

class Menu extends Model
{

    protected $table = "menus";

    protected $fillable = [ 'content', 'restaurant_id' ];

    public static function boot()
    {
        parent::boot();

        static::created(function(Menu $menu) {
            $menu->index($menu);
        });
    }

    /**
     * Returns the restaurant of menu
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant', 'original_id', 'restaurant_id');
    }

    /**
     * @param $item
     */
    public function index($item)
    {
        foreach ( explode("(|)", $item->content) as $menu ) {
            try {
                $string_mod = cleanMenuString($menu);
                foreach ( explode(" ", $string_mod) as $menu ) {
                    if ( !empty( $menu ) ) {
                        $menu = translate(trim(rtrim($menu)));
                        if ( !Index::where('content', $menu)->exists() )
                            Index::create([
                                'content'    => $menu,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ]);
                    }
                }
            } catch ( QueryException $ex ) {

            }
        }
    }
}
