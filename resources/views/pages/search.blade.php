@extends('app')

@section('headers')
    <link rel="stylesheet" href="../assets/vendor/jslider/css/jslider.css">
    <link rel="stylesheet" href="../assets/vendor/jslider/css/jslider.blue.css">
@stop

@section('content')
    <h1 class="page-header text-center">Iskanje restavracij</h1>
    <div class="col-xs-12 col-md-8 col-md-push-2">
        {!! Form::open(['url' => 'searchAdvanced', 'class' => 'form-horizontal', 'method' => 'get']) !!}
        <div class="form-group">
            {!! Form::label('name', 'Ime', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-10">
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('address', 'Naslov', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-10">{!! Form::text('address', null, ['class' => 'form-control']) !!}</div>
        </div>
        <div class="form-group min-margin">
            {!! Form::label('city', 'Kraj', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-10">
                <select name="city" id="city" class="form-control">
                    <option></option>
                    @foreach($cities as $city)
                        <option>{{ $city['city'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('slider', 'Cena', ['style' => 'margin-bottom: 10px;', 'class' => 'col-sm-2']) !!}
            <div class="col-sm-10">
                <input type="slider" id="slider" name="price" value="{{ $prices['min'] }};{{ $prices['max'] }}">
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('features', 'Lastnosti', ['class' => 'col-sm-2', 'style' => 'margin-top:15px;']) !!}
            <div class="col-sm-10 product-chooser">
                @foreach($features as $feature)
                    @include('restaurants.partials.feature')
                @endforeach
            </div>
        </div>
        <div class="form-group col-xs-12 top-margin">
            <button type="submit" class="btn btn-success hidden-xs"><span class="icon icon-search"></span> Išči
            </button>
            <button type="submit" class="btn btn-success center-block visible-xs"><span
                        class="icon icon-search"></span> Išči
            </button>
        </div>
        {!! Form::close() !!}
    </div>
@stop

@section('scripts')
    <script>
        $(function () {
            $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
                if ($(this).hasClass("selected")) {
                    $(this).removeClass('selected');
                    $(this).find('input[type="checkbox"]').prop("checked", false);
                } else {
                    $(this).addClass('selected');
                    $(this).find('input[type="checkbox"]').prop("checked", true);
                }
            });
        });
    </script>
    <script type="text/javascript" src="./assets/vendor/jslider/js/jshashtable-2.1_src.js"></script>
    <script type="text/javascript" src="./assets/vendor/jslider/js/jquery.numberformatter-1.2.3.js"></script>
    <script type="text/javascript" src="./assets/vendor/jslider/js/tmpl.js"></script>
    <script type="text/javascript" src="./assets/vendor/jslider/js/jquery.dependClass-0.1.js"></script>
    <script type="text/javascript" src="./assets/vendor/jslider/js/draggable-0.1.js"></script>
    <script type="text/javascript" src="./assets/vendor/jslider/js/jquery.slider.js"></script>
    <script>
        $(window).ready(function () {
            jQuery("input[type=slider]").slider
            ({
                from: {{ $prices['min'] }},
                to: {{ $prices['max'] }},
                limits: true,
                round: 3,
                step: 0.05,
                format: {locale: 'de'},
                dimension: '&nbsp;€'
            });
        });
    </script>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@stop