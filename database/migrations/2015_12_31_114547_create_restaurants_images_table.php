<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRestaurantsImagesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->increments("id");
            $table->boolean('primary');
            $table->string('restaurant_id');
            $table->foreign('restaurant_id')->references('original_id')->on('restaurants')->onUpdate('cascade')->onDelete('cascade');
            $table->text('prefix');
            $table->text('suffix');
            $table->string('foursquare_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('images', function (Blueprint $table) {
            $table->dropForeign('images_restaurant_id_foreign');
        });
        Schema::drop('images');
    }
}
