<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ratings extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function( Blueprint $table ) {
            $table->increments("id");
            $table->integer("rate");
            $table->integer("user_id")->unsigned();
            $table->string("restaurant_id");
            $table->integer("rating_type_id")->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('restaurant_id')->references('original_id')->on('restaurants')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('rating_type_id')->references('id')->on('rating_types')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ratings', function( Blueprint $table ) {
            $table->dropForeign('ratings_user_id_foreign');
            $table->dropForeign('ratings_restaurant_id_foreign');
            $table->dropForeign('ratings_rating_type_id_foreign');
        });

        Schema::drop('ratings');
    }
}
