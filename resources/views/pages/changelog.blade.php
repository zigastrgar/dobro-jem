@extends('app')

@section('content')
    <h1 class="page-header text-center">Spremembe na Dobro jem</h1>
    <div id="timeline">
        <div class="timeline-item">
            <div class="timeline-icon">
                <div class="icon icon-search-find timelineicon"></div>
            </div>
            <div class="timeline-content">
                <h3>Boljše iskanje <small>14. julij 2016 13:37</small></h3>
                <p>
                    Fantje so zopet naredili nekaj odličnega in sicer so popolnoma spremenili način iskanja, tako da bi sedaj morali najti veliko več stvari kot prej :) Določene modifikacije iskanja še sledijo ;)
                </p>
            </div>
        </div>
        <div class="timeline-item">
            <div class="timeline-icon">
                <div class="icon icon-tachometer timelineicon"></div>
            </div>
            <div class="timeline-content right">
                <h3>Pohitritev za aplikacije <small>13. julij 2016 14:37</small></h3>
                <p>
                    Naš priden razvijalec, ki skrbi za komunikacijo med spletno stranjo in mobilno aplikacijo je postoril nekaj čudovitih stvari glede komunikacije, kar bi sedaj moralo pohitriti nalaganje vsebine v mogilni aplikaciji :)
                </p>
            </div>
        </div>
        <div class="timeline-item">
            <div class="timeline-icon">
                <div class="icon icon-pencil timelineicon"></div>
            </div>
            <div class="timeline-content">
                <h3>Nov izgled restavracije <small>8. julij 2016 21:25</small></h3>
                <p>
                    Da bi čimbolj poenotili sam izgled spletne strani smo tudi spremenili kako izgleda podoben pregled restavracije.
                </p>
            </div>
        </div>
        <div class="timeline-item">
            <div class="timeline-icon">
                <div class="icon icon-pencil timelineicon"></div>
            </div>
            <div class="timeline-content right">
                <h3>Nov izgled "kartice" restavracije <small>8. julij 2016 21:20</small></h3>
                <p>
                    Ekipa je naredila nov dizajn kao izgleda "kartica" restavracije. Prej smo vam prikazali sliko restavracije in določene lastnosti, sedaj pa vam omogočamo hiter pregled cene, imena, lokacije, ocene, komentarjev, razdalje in kaj specifično restavracija ponuja (pizza, solata, hitra hrana, kosila, ...)
                </p>
            </div>
        </div>
        <div class="timeline-item">
            <div class="timeline-icon">
                <div class="icon icon-star timelineicon"></div>
            </div>
            <div class="timeline-content">
                <h3>Huraaa! Končno! <small>18. marec 2016 21:00</small></h3>
                <p>
                    Dobro jem postane javno dostopen :)
                </p>
            </div>
        </div>
    </div>
@stop