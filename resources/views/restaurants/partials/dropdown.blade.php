<div class="visible-xs"><br /><br/></div>
{!! Form::open(['method' => 'GET', 'id' => 'order', 'style' => 'margin-top:-50px;']) !!}
<select data-style="btn-brand" class="selectpicker pull-right" data-width="fit" name="order" id="order">
    <option @if(Request::get('order') == "distance_asc" || !Request::has('order')) selected @endif data-icon="icon-move-up" value="distance_asc">Po oddaljenosti naraščujoče</option>
    <option @if(Request::get('order') == "distance_desc") selected @endif data-icon="icon-move-down" value="distance_desc">Po oddaljenosti padajoče</option>
    <option @if(Request::get('order') == "price_desc") selected @endif data-icon="icon-sort-numeric-desc" value="price_desc">Po ceni padajoče</option>
    <option @if(Request::get('order') == "price_asc") selected @endif data-icon="icon-sort-numeric-asc" value="price_asc">Po ceni naraščujoče</option>
    <option @if(Request::get('order') == "name_asc") selected @endif data-icon="icon-sort-alpha-asc" value="name_asc">Po imenu naraščujoče</option>
    <option @if(Request::get('order') == "name_desc") selected @endif data-icon="icon-sort-alpha-desc" value="name_desc">Po imenu padajoče</option>
</select>
{!! Form::close() !!}
<div class="clearfix"></div>