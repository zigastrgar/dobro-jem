<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use Illuminate\Http\Response as IlluminateResponse;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller
{

    protected $statusCode = 200;

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function respondNotFound($message = "Objekt ni najden!")
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND)->respondWithError($message);
    }

    public function respondWithError($message, $errors = null)
    {
        return $this->respond(array_merge([
            'error' => [
                'message' => $message,
                'status'  => $this->getStatusCode()
            ]
        ], [ 'validation_errors' => ( count($errors) == 0 ) ? null : array_values($errors->toArray()) ]));
    }

    public function respond($data, $headers = [ ])
    {
        return Response::json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithValidationErrors($message = "Napaka pri pregledu podatkov!", $errors = [ ])
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY)->respondWithError($message, $errors);
    }

    public function respondInternalError($message = "Neznana napaka na strežniku", $errors = [ ])
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)
            ->respondWithError($message, $errors);
    }

    public function respondNoContent()
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_NO_CONTENT)->respond([ ]);
    }
}
