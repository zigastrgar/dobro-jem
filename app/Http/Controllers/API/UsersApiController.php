<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Auth\AuthController as AController;

class UsersApiController extends ApiController
{
    public function register(AController $authController, Request $request)
    {
        $email     = $request->input('email');
        $password  = $request->input('password');
        $password2 = $request->input('password_verify');
        $username  = $request->input('username');
        $address   = $request->input('address');

        $lat = null;
        $lng = null;
        if ( !empty( $address ) )
            list( $lat, $lng ) = addressToGeoLocation($address);

        $json = [ ];
        if ( !empty( $email ) && !empty( $password ) ) {
            if ( !User::where('username', $username)->exists() ) {
                if ( !User::where('email', $email)->exists() ) {
                    if ( $password == $password2 ) {
                        $status = $authController->isConsideredAdmin($email);
                        $user   = User::create([
                            'email'      => $email,
                            'password'   => bcrypt($password),
                            'first_time' => true,
                            'status'     => $status,
                            'address'    => $address,
                            'lat'        => $lat,
                            'lng'        => $lng,
                            'username'   => $username
                        ]);
                        if ( !is_null($user) ) {
                            return $this->respondNoContent();
                        } else {
                            $json["message"] = "Napka pri vnosu v bazo!";
                        }
                    } else {
                        $json["message"] = "Gesli se ne ujemata!";
                    }
                } else {
                    $json["message"] = "Uporabnik s takšnim e-naslovom že obstaja!";
                }
            } else {
                $json["message"] = "Uporabnik s takšnim uporabniškim imenom že obstaja!";
            }
        } else {
            $json["message"] = "E-naslov in geslo sta obvezna!";
        }

        return $this->respondWithError($json['message']);
    }

    public function login(Request $request)
    {
        $email    = $request->input('email');
        $password = $request->input('password');

        $json = [ ];

        if ( User::where('email', $email)->exists() ) {
            if ( Auth::attempt([ 'email' => $email, 'password' => $password ]) ) {
                $user             = Auth::user();
                $json['user']     = [
                    'name'     => $user['name'],
                    'address'  => $user['address'],
                    'username' => $user['username'],
                    'email'    => $user['email'],
                    'lat'      => (double)$user['lat'],
                    'lng'      => (double)$user['lng']
                ];
                $json["response"] = true;
                $json["message"]  = "Prijava uspešna!";

                return $this->respond($json);
            } else {
                return $this->respondWithError("Napačno geslo!");
            }
        } else {
            return $this->respondWithError("Uporabnik s takšnim e-naslovom ne obstaja!");
        }
    }

    public function social(AController $auth, Request $request)
    {
        $provider = $request->input('provider');
        $email    = $request->input('email');
        $name     = $request->input('name');

        $user = User::where('provider', $provider)->where('email', $email)->first();

        $user = User::findOrFail($user->id);

        $json = [ ];

        if ( !is_null($user) ) {
            Auth::login($user, true);
            $user             = Auth::user();
            $json["response"] = true;
            $json["message"]  = "Prijava uspešna!";
        } else {
            $user             = User::create([
                'name'       => $name,
                'email'      => $email,
                'provider'   => $provider,
                'status'     => $auth->isConsideredAdmin($email),
                'first_time' => true
            ]);
            $json["response"] = true;
            $json["message"]  = "Prijava uspešna!";
        }

        $json['user'] = [
            'name'     => $user['name'],
            'address'  => $user['address'],
            'username' => $user['username'],
            'email'    => $user['email'],
            'lat'      => (double)$user['lat'],
            'lng'      => (double)$user['lng']
        ];

        return $this->respond($json);
    }
}
