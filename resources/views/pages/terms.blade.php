@extends('app')

@section('content')
    <h1 class="page-header text-center">Pogoji uporabe strani</h1>
    <div class="col-xs-12 col-sm-8 col-sm-push-2">
        <h2 class="page-header text-center" id="general">Splošni pogoji</h2>
        <p>
            Splošni pogoji poslovanja in uporabe so sestavljeni v skladu z <a href="http://www.pisrs.si/Pis.web/pregledPredpisa?id=ZAKO3906" target="_blank">zakonom o varstvu osebnih podatkov</a> in <a href="http://www.pisrs.si/Pis.web/pregledPredpisa?id=ZAKO513" target="_blank">zakonom o varstvu potrošnikov</a>.
        </p>
        <p>
            Z uporabo spletne strani dobro-jem.si uporabniki izjavljajo, da se strinjajo z navedenimi pogoji in pravili ter pristajajo na njihovo uporabo v skladu z njimi.
        </p>
        <p>
            Ob registraciji uporabnik vpiše e-naslov in geslo, ki jih uporabi ob prijavi. E-naslov in geslo uporabnika nedvoumno določata in ga povezujeta z vnesenimi podatki. Z registracijo uporabnik pridobi pravico do uporabe naših storitev. Ali pa za prijavo uporabi prijavo s socialnim profilom (Facebook, Twitter, GitHub).
        </p>
        <p>
            Uporabnik sam je osebno odgovoren za varovanje gesla.
        </p>
        <p>
            Uporabnik je seznanjen, da lahko občasno pride do začasne prekinitve uporabe storitve zaradi objektivnih ali subjektivnih razlogov. Prav tako si pridržujemo pravico rednega vzdrževanja spletne strani in v tem času morda ne bo na voljo uporabnikom (začasno nedostopna ali izključena).
        </p>
        <p>
            Pridržujemo si pravico, da kadarkoli določeno storitev doda ali odstrani brez omejitev.
        </p>
        <h2 class="page-header text-center">Spreminjanje pogojev</h2>
        <p>
            Podjetje si pridržuje pravico, da kadarkoli brez odpovednega roka ukine, doda ali spremeni kateregakoli od navedenih splošnih ali drugih posebnih pogojev uporabe. Uporabnikom svetujemo, da redno prebirajo vse dokumente, da bi bili obveščeni o spremembah. V kolikor pride do sprememb teh dokumentov in od tega trenutka še naprej uporabljate naše storitve, pomeni, da se strinjate s temi spremenjenimi postavkami, ki so trenutno veljavne. Vsaka sprememba ali izbris postavke začne veljati takoj, ko je objavljena na spletni strani.
        </p>
        <h2 class="page-header text-center">Uporabnik</h2>
        <p>
            Uporabniki morajo pri uporabi spletne strani vedeti, da so njihove dejavnosti v skladu z veljavnimi predpisi v Republiki Sloveniji.
        </p>
        <p>
            Uporabniki v svojih dejavnostih ne smejo objavljati ali posredovati kakršnega koli materiala, ki ogroža ali krši pravice tretjih oseb, gradiva, ki je po svoji naravi nezakonito, ogroža, je žaljivo, obrekljivo, ki izkrivlja resnico ali ogroža zasebnost, ki je vulgarno, opolzko ali je na kakšen drug način nezaželeno ali prepovedano v predmetni komunikaciji; materiala, ki spodbuja nezakonite dejavnosti ali kako drugače krši katere koli predpise.
        </p>
        <h2 class="page-header text-center" id="control">Nadzor vsebine in uporabnikov</h2>
        <p>
            Podjetje si pridržuje pravico do nadzora nad vsebinami. Hkrati pa ta pravica ni obveznost nadzora. Podjetje si tako pridržuje pravico, da odstrani vse gradivo ali uporabnike, za katerega po lastni presoji ugotovi, da ni v skladu s pravili, ki so določena tukaj ali ki, se mu objektivno ali subjektivno zdijo vprašljiva ali je kakor koli prekršil pogoje uporabe.
        </p>
        <p>Vse oddaljenosti med uporabnikom in restavracijo so merjene v zračni razdalji! Med računanjem oddaljenosti pa lahko pride do napake v primeru nenatančne lokacije restavracije ali uporabnika.</p>
        <p>
            Podjetje ni odgovorno za napačne podatke o restavraciji (ime, naslov, lokacija, meniji), saj se podatki osvežujejo dnevno! Tudi do trikrat (7:00, 8:00, 9:00) ob uvozu se lahko zgodi tudi da se določene restavracije še niso posodobile in bodo prikazani napačni meniji za ta dan.
        </p>
        <h2 class="page-header text-center" id="privacy">Zasebnost</h2>
        <p>
            Vsi osebni podatki, ki jih uporabnik vpiše ne bodo nikoli posredovani tretjim osebam in bodo izklučno uporabljene za poslovne namene (komunikacija).
        </p>
        <h2 class="page-header text-center" id="limit">Omejitev odgovornosti</h2>
        <p>Ne prevzemamo odgovornosti za verodostojnos, pravilnost ali celovitost podatkov restavracij, menijev in ostalih podatkov na strani. Storitev Dobro jem si ne lasti nobene izmed slik na spletni strani.</p>
    </div>
@stop