@extends('home')

@section('content')
    <div class="col-xs-12 col-md-8 col-md-push-2">
        {!! Form::open(['url' => 'search', 'method' => 'get']) !!}
        <div class="form-group form-group-lg">
            <div class="input-group">
                <input class="form-control normal-line-height" type="search" name="search" placeholder="Iskanje..."/>
                <span class="input-group-btn">
        <button style="line-height: 1.4;" class="btn btn-lg btn-default" type="submit"><span class="icon icon-search"></span></button>
      </span>
            </div>
        </div>
        {!! Form::close() !!}
        <div id="results" class="text-left">
            <div class="col-xs-12">
                <h2 class="min-margin text-center">Naključno</h2>
                @foreach($restaurants as $restaurant)
                    <div class="col-xs-12 col-sm-6">
                        @include('restaurants.partials.restaurant_list')
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop

@section('scripts')
    @include('restaurants.partials.like_script')
@stop