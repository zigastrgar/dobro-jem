<?php

namespace App\Http\Controllers\API;

use App\Restaurant;
use DobroJem\Transformers\RestaurantTransformer;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class RestaurantsApiController extends ApiController
{
    protected $restaurantTransformer;

    public function __construct(RestaurantTransformer $restaurantTransformer)
    {
        $this->restaurantTransformer = $restaurantTransformer;
    }

    public function index(Request $request)
    {
        $params   = $request->all();
        $order_by = $this->getOrderMethod($params);

        $restaurants = Cache::remember('restaurants', 60, function() {
            return Restaurant::all();
        });

        $result        = [ ];
        $result['all'] = $restaurants->count();

        $limit = $restaurants->count();
        if ( $request->has('limit') != 0 )
            $limit = $params['limit'];

        if ( $restaurants->count() > 0 ) {
            foreach ( $restaurants->forPage(1, $limit) as $restaurant ) {
                $transformed             = $this->restaurantTransformer->transform($restaurant);
                $distance
                                         = calculateDistance($request->input('lat'), $request->input('lng'), $restaurant->lat, $restaurant->lng);
                $result['restaurants'][] = array_merge($transformed, [
                    'distance'        => humanizeDistance($distance),
                    'name_cmp'        => translateName($restaurant->name),
                    'distance_metres' => $distance,
                    'rating'          => getRating($restaurant->ratings()->lists('rate')),
                    'comments'        => count($restaurant->comments->toArray()),
                    'time_remaining'  => timeRemaining($restaurant),
                    'opened'          => getStateOfRestaurant($restaurant),
                    'image'           => ( !is_null($restaurant->primaryImage) ) ? imageUrl($restaurant->primaryImage->prefix, $restaurant->primaryImage->suffix) : null,
                    'features'        => $restaurant->features()->lists('feature_id')
                ]);
            }

            usort($result['restaurants'], $order_by);
            $result['restaurants'] = cleanUp($result['restaurants'], [ "name_cmp", "distance_metres" ]);

            return $this->respond($result);
        }

        return $this->respondInternalError("Whooops, naš programer Benjamin štrajka in ni več restavracij :(");
    }

    public function show($id, Request $request)
    {
        $restaurant = Restaurant::all()->where('id', $id)->first();
        if ( is_null($restaurant) ) {
            return $this->respondNotFound('Oprostite, ampak ta restavracija ne obstaja!');
        }

        $result                        = [ ];
        $logged                        = $request->input('logged');
        $restaurant                    = Restaurant::withoutGlobalScope('distance')->where('id', $id)->first();
        $result['restaurant']          = array_merge($this->restaurantTransformer->transform($restaurant), [
            'distance'       => humanizeDistance(calculateDistance($request->input('lat'), $request->input('lng'), $restaurant->lat, $restaurant->lng)),
            'time_remaining' => timeRemaining($restaurant),
            'opened'         => getStateOfRestaurant($restaurant),
            'weekdays'       => formatDate($restaurant->opening_week),
            'saturdays'      => formatDate($restaurant->opening_sat),
            'sundays'        => formatDate($restaurant->opening_sun)
        ]);
        $result['restaurant']['menus'] = [ ];
        $menus                         = $restaurant->menus->toArray();
        foreach ( $menus as $menu ) {
            $foods = [ ];
            foreach ( explode("(|)", $menu['content']) as $food ) {
                if ( !empty( $food ) ) {
                    $foods[] = $food;
                }
            }
            $result['restaurant']['menus'][] = $foods;
        }

        if ( $logged == 1 ) {
            if ( count($restaurant->comments->toArray()) > 0 ) {
                foreach ( $restaurant->comments as $comment ) {
                    $result['restaurant']['comments'][] = [
                        'content'  => $comment->content,
                        'username' => $this->pickName($comment->user),
                        'created'  => $comment->created_at
                    ];
                }
            } else {
                $result['restaurant']['comments'] = "Ni komentarjev!";
            }
        } else {
            $result['restaurant']['comments'] = count($restaurant->comments->toArray());
        }

        $result['restaurant']['ratings']['overall'] = getRating($restaurant->ratings()->lists('rate'));

        foreach ( [ 'quality', 'clean', 'ambient', 'service' ] as $type ) {
            $result['restaurant']['ratings'][$type] = getRating($restaurant->ratings()
                ->where('rating_type_id', getRateTypeId($type))->lists('rate'));
        }

        return $this->respond($result);
    }

    public function offline(Request $request)
    {
        $restaurants   = Restaurant::all();
        $result        = [ ];
        $all           = $restaurants->count();
        $result['all'] = $all;

        $limit = $all;
        if ( $request->input('limit') != 0 )
            $limit = $request->input('limit');


        if ( $all > 0 ) {
            foreach ( $restaurants->forPage(1, $limit) as $restaurant ) {
                $result['restaurants'][] = $this->restaurantTransformer->transform($restaurant);
            }
            $result['sent'] = $limit;

            return $this->respond($result);
        }

        return $this->respondInternalError("Whooops, naš programer Benjamin štrajka in ni več restavracij :(");
    }

    public function menus($id)
    {
        if ( !Restaurant::all()->where('id', $id)->exists() ) {
            return $this->respondNotFound('Oprostite, ampak ta restavracija ne obstaja!');
        }

        $menus = DB::table('menus')->where(function($q) use ($id) {
            $q->whereRaw("restaurant_id = (SELECT original_id FROM restaurants WHERE id = {$id})");
        })->pluck('content');

        foreach ( $menus as $menu ) {
            $result[] = explode("(|)", $menu);
        }

        return $this->respond($result);
    }

    private function getOrderMethod($params)
    {
        $order = "distance_asc";

        if ( !empty( $params['order'] ) ) {
            $order = $params['order'];
        }

        list( $name, $direction ) = explode("_", $order);

        switch ( $name ) {
            case "distance":
                $order_by = "sortByDistance";
                break;
            case "name":
                $order_by = "sortByName";
                break;
            case "rating":
                $order_by = "sortByRating";
                break;
            case "id":
                $order_by = "sortById";
                break;
            default:
                $order_by = "sortByDistance";
                break;
        }

        if ( $direction == "desc" ) {
            $order_by .= "Desc";
        }

        return $order_by;
    }

    public function miniRestaurants()
    {
        $restaurants = Cache::remember('restaurants', 60, function() {
            return Restaurant::all();
        });

        $result = [ ];

        if ( $restaurants->count() > 0 ) {
            foreach ( $restaurants as $restaurant ) {
                $transformed = $this->restaurantTransformer->transform($restaurant, true);
                $result[]    = array_merge($transformed, [
                    'rating'   => getRating($restaurant->ratings()->lists('rate')),
                    'comments' => count($restaurant->comments->toArray()),
                    'opened'   => getStateOfRestaurant($restaurant),
                    'features' => $restaurant->features()->lists('feature_id')
                ]);
            }

            return $this->respond($result);
        }

        return $this->respondInternalError("Whooops, naš programer Benjamin štrajka in ni več restavracij :(");
    }

    private function pickName($user)
    {
        return ( strlen($user->username) > 0 ) ? $user->username : $user->email;
    }
}
