<?php

namespace DobroJem\Transformers;

class RestaurantTransformer extends Transformer
{

    public function transform($restaurant, $mini = false)
    {
        $array = [
            'id'      => (int)$restaurant['id'],
            'name'    => $restaurant['name'],
            'address' => $restaurant['address'],
            'price'   => price(trim($restaurant['value_of_charge']), '€'),
            'city'    => trim($restaurant['city']),
            'lat'     => (double)$restaurant['lat'],
            'lng'     => (double)$restaurant['lng']
        ];
        if ( $mini == false ) {
            foreach ( explode(",", $restaurant['phone']) as $phone ) {
                $array['phones'][] = phoneParse($phone);
            }
        }

        return $array;
    }
}