<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = "features";

    protected $fillable = [ 'name', 'created_at', 'updated_at', 'picture', 'description' ];

    /**
     * Returns all the restaurants with given feature
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function restaurants()
    {
        return $this->belongsToMany('App\Restaurant', 'feature_restaurant', 'feature_id', 'restaurant_id')
            ->withTimestamps();
    }
}
