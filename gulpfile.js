var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', './public/assets/css');
    mix.version('./public/assets/css/app.css');
});

//var gulp = require('gulp');
//var imageop = require('gulp-image-optimization');
//
//gulp.task('images', function(cb) {
//    gulp.src(['./public/assets/img/*.png','./public/assets/img/*.jpg','./public/assets/img/*.gif','./public/assets/img/*.jpeg']).pipe(imageop({
//        optimizationLevel: 15,
//        progressive: true,
//        interlaced: true
//    })).pipe(gulp.dest('public/assets/img')).on('end', cb).on('error', cb);
//});

/*var gulp = require('gulp');
var gs = require('gulp-image-grayscale');

gulp.task('gs', function () {
    return gulp.src('./public/assets/img/resized/!*.*')
        .pipe(gs({
            logProgress: true
        }))
        .pipe(gulp.dest('./public/assets/img/gray'));
});

var imageResize = require('gulp-image-resize');

gulp.task('resize', function () {
    gulp.src('./public/assets/img/img/!*.*')
        .pipe(imageResize({
            width : 800,
            height : 520,
            crop : true,
            upscale : false
        }))
        .pipe(gulp.dest('./public/assets/img/resized'));
});*/

//gulp.task('default', ['gs', 'resize']);
