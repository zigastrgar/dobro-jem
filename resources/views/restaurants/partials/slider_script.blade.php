<script type="text/javascript" src="./assets/vendor/jslider/js/jshashtable-2.1_src.js"></script>
<script type="text/javascript" src="./assets/vendor/jslider/js/jquery.numberformatter-1.2.3.js"></script>
<script type="text/javascript" src="./assets/vendor/jslider/js/tmpl.js"></script>
<script type="text/javascript" src="./assets/vendor/jslider/js/jquery.dependClass-0.1.js"></script>
<script type="text/javascript" src="./assets/vendor/jslider/js/draggable-0.1.js"></script>
<script type="text/javascript" src="./assets/vendor/jslider/js/jquery.slider.js"></script>
<script>
    $(window).ready(function () {
        jQuery("input[type=slider]").slider
        ({
            from: 500,
            to: 5000,
            step: 100,
            format: {locale: 'de'},
            dimension: '&nbsp;m'
        });
    });
</script>