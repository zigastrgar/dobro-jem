@extends('app')

@section('content')
    <div class="col-xs-12">
        <h1 class="page-header text-center">Urejanje slik
            <small>{{ $restaurant->name }}</small>
        </h1>
        <?php
        $images = $restaurant->images->toArray();
        ?>
            <a target="_blank" href="https://foursquare.com/v/{{ $images[0]['foursquare_id'] }}">Poglej na foursquare</a>
        <div class="col-xs-12 product-chooser" style="margin: 0;">
            @foreach($restaurant->images as $image)
                <div id="image{{ $image->id }}"
                     class="col-xs-12 col-sm-6 col-md-4 product-chooser-item @if($image->primary == true) selected @endif"
                     style="margin: 0;">
                    <img src="{{ imageUrl($image->prefix, $image->suffix) }}" alt="" class="img-responsive">
                    <div class="col-xs-12">
                        <input type="radio" @if($image->primary == true) selected @endif name="image"
                               value="{{ $image->id }}" data-restaurant-id="{{ $restaurant->original_id }}">
                    </div>
                    <span onClick="deleteImage({{ $image->id }}, {{ $image->restaurant_id }})" class="text-danger"><span
                                class="icon icon-times"></span></span>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('div.product-chooser').not('.disabled').find('div.product-chooser-item').on('click', function () {
                $('div.product-chooser-item').removeClass('selected');
                $(this).addClass('selected');
                $(this).find('input[type="radio"]').prop("checked", true);
                console.log($(this));
                $.ajax({
                    url: "/editImages",
                    type: "POST",
                    data: {
                        value: $(this).find('input[type="radio"]').val(),
                        restaurant: $(this).find('input[type="radio"]').attr('data-restaurant-id'),
                        _token: '{{ csrf_token() }}'
                    },
                    success: function () {

                    }
                });
            });
        });

        function deleteImage(id, restaurant) {
            $.ajax({
                url: "/deleteImage",
                type: "POST",
                data: {
                    id: id,
                    restaurant: restaurant,
                    _token: '{{ csrf_token() }}'
                },
                success: function () {
                    $("#image" + id).remove();
                }
            });
        }
    </script>
@endsection