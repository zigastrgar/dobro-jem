<script>
    $(".favourite").click(function () {
        var id = $(this).attr('data-restaurant-id');
        console.log($(this).attr('data-restaurant-id'));
        $.ajax({
            url: '/like',
            type: 'POST',
            data: {restaurant: id, user: {{ Auth::id() }}, _token: '{{ csrf_token() }}'},
            success: function (cb) {
                cb = cb.split("|");
                if (cb[0] == "success") {
                    if (cb[1] == "add") {
                        $("[data-restaurant-id="+id+"]").removeClass('icon-heart-o').addClass('icon-heart liked');
                    } else {
                        $("[data-restaurant-id="+id+"]").addClass('icon-heart-o').removeClass('icon-heart liked');
                    }
                }
            }
        });
    });
</script>