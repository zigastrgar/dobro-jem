@extends('app')

@section('content')
    <h1 class="page-header text-center">Dodajanje značk za iskanje</h1>

    {!! Form::open(['action' => 'Admin\AdminController@addKeyword']) !!}
    <div class="col-xs-12 col-sm-8 col-sm-push-2">
        <div class="col-xs-12 col-sm-6">
            <div class='form-group'>
                {!! Form::label('keyword', 'Ključna beseda') !!}
                {!! Form::text('keyword', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class='form-group'>
                {!! Form::label('index', 'Obstoječi index') !!}
                {!! Form::text('index', null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-xs-12">
            <button type="submit" class="btn btn-success">Dodaj ključno besedo</button>
        </div>
    </div>
    {!! Form::close() !!}
    <div class="clearfix"></div>
    <h2 class="page-header text-center">Obstoječe povezave</h2>
    <div class="col-xs-12 col-sm-8 col-sm-push-2">
        <table class="table table-bordered table-condensed">
            <tr>
                <th>
                    Ključna beseda
                </th>
                <th>
                    Indeks
                </th>
            </tr>
            @foreach($keywords as $keyword)
                @foreach($keyword->indexes()->lists('content') as $index)
                    <tr>
                        <td>
                            {{ $keyword->content }}
                        </td>
                        <td>
                            {{ $index }}
                        </td>
                    </tr>
                @endforeach
            @endforeach
        </table>
    </div>
    <div class="clearfix"></div>
    <h2 class="page-header text-center">Iskalni nizi uporabnikov</h2>
    <div class="col-xs-12 col-sm-8 col-sm-push-2">
        <table class="table table-bordered table-condensed">
            <tr>
                <th>
                    Iskalni niz
                </th>
                <th>
                    Število rezultatov
                </th>
                <th>
                    Število iskanj
                </th>
            </tr>
            @foreach($searches as $search)
                <tr>
                    <td>
                        {{ $search->query }}
                    </td>
                    <td>
                        {{ $search->maximum }}
                    </td>
                    <td>
                        {{ $search->number }}
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@stop