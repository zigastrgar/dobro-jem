<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse"
                    aria-expanded="false">
                <span class="sr-only">Meni</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="{{ action('PagesController@index') }}" class="navbar-brand-centered">
                <img src="/assets/img/logoDJ_90.png" width="90" alt="Logo">
            </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ action('PagesController@index') }}"><span class="icon icon-home-1"></span> Domov</a>
                </li>
                <li><a href="{{ action('RestaurantsController@index') }}"><span class="icon icon-restaurant"></span>
                        Restavracije</a></li>
                <li><a href="{{ action('PagesController@nearest') }}"><span class="icon icon-location-arrow"></span>
                        Najbližje</a></li>
                <li><a href="{{ action('PagesController@search') }}"><span class="icon icon-search"></span> Iskanje</a>
                </li>
                <li><a href="{{ action('PagesController@lucky') }}"><span class="icon icon-help"></span> Naključno</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                @if(Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><span class="myName">
                         @if(empty(Auth::user()->username) && empty(Auth::user()->name))
                                    {{ Auth::user()->email}}
                                @elseif(empty(Auth::user()->username))
                                    {{ Auth::user()->name}}
                                @else
                                    {{ Auth::user()->username }}
                                @endif
                         </span>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            @if(Auth::check() && Auth::user()->admin('admin'))
                                <li>
                                    <a href="{{ url('dashboard') }}">Nadzorna plošča</a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ url('editProfile') }}">Uredi profil</a></li>
                            <li>
                                <a href="{{ url('logout') }}">Odjava</a>
                            </li>
                        </ul>
                    </li>
                @else
                    <li><a href="{{ url('login') }}"><span class="icon icon-user"></span> Prijava</a>
                    </li>
                    <li>
                        <a href="{{ url('login') }}"><span class="icon icon-user-add"></span>
                            Registracija</a></li>
                @endif
            </ul>
            {!! Form::open(['url' => 'search', 'class' => 'navbar-form navbar-right', 'role' => 'search', 'method' => 'get']) !!}
            <div class="form-group">
                <div class="input-group">
                    <input type="text" name="search" class="form-control normal-line-height"
                           placeholder="Dej neki na hitr...">
<span class="input-group-btn">
        <button class="btn btn-default" type="submit"><span class="icon icon-search"></span></button>
      </span>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</nav>