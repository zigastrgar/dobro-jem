<a href="#restaurants" class="label label-primary">Restavracije</a>
<a href="#restaurant" class="label label-primary">Restavracija</a>
<a href="#register" class="label label-primary">Registracija</a>
<a href="#login" class="label label-primary">Prijava</a>
<a href="#social" class="label label-primary">Prijava preko socialnih omrežij</a>
<a href="#comment" class="label label-primary">Komentiranje</a>
<a href="#rate" class="label label-primary">Ocenjevanje</a>
<a href="#favourite" class="label label-primary">Priljubljeno</a>
<a href="#search" class="label label-primary">Iskanje</a>
<a href="#offline" class="label label-primary">Offline</a>