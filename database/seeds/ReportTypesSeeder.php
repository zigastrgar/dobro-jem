<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReportTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('report_types')->insert(['title' => 'Tehnična napaka']);
        DB::table('report_types')->insert(['title' => 'Pohvala/Predlog :)']);
        DB::table('report_types')->insert(['title' => 'Ime restavracije']);
        DB::table('report_types')->insert(['title' => 'Lokacija restavracije']);
        DB::table('report_types')->insert(['title' => 'Napaka v meniju']);
        DB::table('report_types')->insert(['title' => 'Slika']);
        DB::table('report_types')->insert(['title' => 'Neprimeren komentar']);
        DB::table('report_types')->insert(['title' => 'Neprimereno ime uporabnika']);
        DB::table('report_types')->insert(['title' => 'Slovnična napaka']);
        DB::table('report_types')->insert(['title' => 'Drugo']);
    }
}
