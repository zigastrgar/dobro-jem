<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RatingType extends Model
{
    protected $table = "rating_types";

    protected $fillable = [ 'name' ];

    public function rates()
    {
        return $this->hasMany('App\Rating');
    }
}
