@extends('app')

@section('content')
    <div class="col-xs-12">
        <h1 class="page-header text-center">
            Dokumentacija - Dobro jem API
            <small>V2</small>
        </h1>
        @include('pages.api.partials.labels')
        @include('pages.api.partials.title', ['data' => ['name' => 'Restavracije', 'id' => 'restaurants']])
        @include('pages.api.partials.title', ['data' => ['name' => 'Restavracija', 'id' => 'restaurant', 'slug' => '{id}']])
        @include('pages.api.partials.title', ['data' => ['name' => 'Registracija', 'id' => 'register']])
        @include('pages.api.partials.title', ['data' => ['name' => 'Prijava', 'id' => 'login']])
        @include('pages.api.partials.title', ['data' => ['name' => 'Prijava preko socialnih omrežij', 'id' => 'social']])
        @include('pages.api.partials.title', ['data' => ['name' => 'Komentiranje', 'id' => 'comment']])
        @include('pages.api.partials.title', ['data' => ['name' => 'Ocenjevanje', 'id' => 'rate']])
        @include('pages.api.partials.title', ['data' => ['name' => 'Priljubljeno', 'id' => 'favourite']])
        @include('pages.api.partials.title', ['data' => ['name' => 'Iskanje', 'id' => 'search']])
        @include('pages.api.partials.title', ['data' => ['name' => 'Offline', 'id' => 'offline']])
    </div>
@stop