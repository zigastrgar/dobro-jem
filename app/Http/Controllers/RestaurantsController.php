<?php

namespace App\Http\Controllers;


use Request;
use App\Http\Requests;
use App\Restaurant;
use Auth;
use SEO;
use SEOMeta;

class RestaurantsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        SEO::setTitle('Restavracije');
        SEO::opengraph()->setUrl(url()->full());
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite('@dobrojem');

        $restaurants = Restaurant::distance()->ordering(Request::get('order'))->paginate(18);

        return view('restaurants.index', compact('restaurants'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $restaurant = Restaurant::where('slug', $id)->first();

        SEO::setTitle($restaurant->name);
        SEOMeta::addKeyword([
            'preharana',
            'študentska',
            'študentska prehrana',
            'študentje',
            'boni',
            'malica',
            'obrok',
            'restavracija',
            'Ljubljana',
            'Maribor',
            'Celje',
            'študetnski boni',
            'poceni',
            'dobro',
            'dobro jem',
            'študenti',
            $restaurant->name,
            $restaurant->city,
            $restaurant->slug
        ]);
        SEO::opengraph()->setUrl(url()->full());
        SEO::opengraph()->addProperty('type', 'website');
        SEO::twitter()->setSite('@dobrojem');

        return view('restaurants.show', compact('restaurant'));
    }
}
