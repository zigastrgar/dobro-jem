@extends('app')

@section('headers')
    <link rel="stylesheet" href="../assets/vendor/jslider/css/jslider.css">
    <link rel="stylesheet" href="../assets/vendor/jslider/css/jslider.blue.css">
@stop

@section('content')
    @include('restaurants.partials.slider')
    <div itemscope itemtype="http://schema.org/Restaurant">
        <div class="col-xs-12">
            <h1 class="page-header text-center"><span itemprop="name">{{ $restaurant->name }}</span><br/>
                <small itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><span
                            itemprop="streetAddress">{{ $restaurant->address }}</span>, <span
                            itemprop="addressLocality">{{ $restaurant->city }}</span>
                    (<span data-toggle="tooltip" data-html="true" data-placement="bottom"
                           title="Pri izračunu oddaljenosti lahko pride do napake">
                @if(Auth::check() && strlen(Auth::user()->lat) > 0 && strlen(Auth::user()->lng))
                            {{ humanizeDistance(calculateDistance($restaurant->lat, $restaurant->lng, Auth::user()->lat, Auth::user()->lng)) }}
                        @else
                            {{ humanizeDistance(calculateDistance($restaurant->lat, $restaurant->lng, session('lat'), session('lng'))) }}
                        @endif
                </span>)
                </small>
            </h1>
        </div>
        <div class="col-xs-12 col-md-6 col-md-push-3 text-center">
            <div class="col-xs-4 col-md-4">
                <span class="icon icon-phone block"></span>
                <p itemprop="telephone" data-toggle="tooltip" data-html="true" data-placement="bottom"
                   title="{{ otherPhoneNumbers($restaurant->phone) }}">{{ phoneParse($restaurant->phone) }}</p>
            </div>
            <div class="col-xs-4 col-md-4">
                <p class="action-price" itemprop="priceRange">{{ price($restaurant->value_of_charge, "€") }}</p>
            </div>
            <div class="col-xs-4 col-md-4">
                <span class="icon icon-android-time block"></span>
                <p data-toggle="tooltip" data-html="true" data-placement="bottom"
                   title="Med tednom: {{ formatDate($restaurant->opening_week) }} <br />Sobote: {{ formatDate($restaurant->opening_sat) }}<br />Nedelje: {{ formatDate($restaurant->opening_sun) }}">{{ formatDate(returnTimeOfWork($restaurant)) }}</p>
                <meta itemprop="openingHours" content="Po-Pe {{ formatDate($restaurant->opening_week) }}">
                <meta itemprop="openingHours" content="So {{ formatDate($restaurant->opening_sat) }}">
                <meta itemprop="openingHours" content="Ne {{ formatDate($restaurant->opening_sun) }}">
            </div>
        </div>
        @if(count($restaurant->features->toArray()) > 0)
            <div class="features-list">
                @foreach($restaurant->features as $feature)
                    @include('restaurants.partials.feature_restaurant_list')
                @endforeach
            </div>
        @endif
        @include('restaurants.partials.map')
        <div id="menus">
            <h2 class="page-header text-center">Dnevni meniji</h2>
            @include("restaurants.partials.menus")
        </div>
        <div id="rating">
            @include('restaurants.partials.rating')
        </div>
        <div id="comments">
            @include('restaurants.partials.comments')
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    <script src="../assets/vendor/starrr/js/starrr.js"></script>
    @include('restaurants.partials.rating_script')
    @include('restaurants.partials.map_scripts')
    @include('restaurants.partials.slider_script')
@stop