<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Restaurants extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('original_id')->unique()->index();
            $table->string('name')->index();
            $table->string('address')->index();
            $table->string('city')->index();
            $table->float('value_of_charge');
            $table->string('phone')->nullable();
            $table->string('opening_week')->nullable();
            $table->string('opening_sat')->nullable();
            $table->string('opening_sun')->nullable();
            $table->string('opening_note')->nullable();
            $table->double('lat');
            $table->double('lng');
            $table->string('slug')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("restaurants");
    }
}
