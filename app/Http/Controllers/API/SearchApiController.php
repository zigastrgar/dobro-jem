<?php

namespace App\Http\Controllers\API;

use DobroJem\Transformers\RestaurantTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;

class SearchApiController extends ApiController
{

    protected $restaurantTransformer;

    public function __construct(RestaurantTransformer $restaurantTransformer)
    {
        $this->restaurantTransformer = $restaurantTransformer;
    }

    public function search(Request $request)
    {
        $search      = $request->input('search');
        $search_hash = translate($search);
        $user        = (int)$request->input('user');
        $order       = $request->input('order');

        $json = [ ];

        $results = Restaurant::where(function($q) use ($search_hash) {
            $q->whereRaw("original_id IN (SELECT DISTINCT(restaurant_id) FROM relations WHERE index_id IN (SELECT id FROM indexes WHERE content LIKE '%$search_hash%')) OR original_id IN (SELECT DISTINCT(restaurant_id) FROM relations WHERE index_id IN (SELECT index_id FROM index_keyword WHERE keyword_id IN (SELECT id FROM keywords WHERE content LIKE '%$search_hash%')))");
        })->ordering($order)->get();


        foreach ( $results as $restaurant ) {
            $json['results'][] = array_merge($this->restaurantTransformer->transform($restaurant), [
                'distance'       => humanizeDistance(calculateDistance($request->input('lat'), $request->input('lng'), $restaurant->lat, $restaurant->lng)),
                'rating'         => getRating($restaurant->ratings()->lists('rate')),
                'comments'       => count($restaurant->comments->toArray()),
                'time_remaining' => timeRemaining($restaurant),
                'opened'         => getStateOfRestaurant($restaurant)
            ]);
        }

        $json['query'] = $search;
        $json['count'] = count($results);

        $type = DB::table('api_keys')->select('type')->where('key', Request::get('api_key'))->first();

        Search::insert([
            'query'      => $search_hash,
            'results'    => count($results),
            'user_id'    => $user,
            'agent'      => $_SERVER["HTTP_USER_AGENT"],
            'ip'         => $_SERVER["REMOTE_ADDR"],
            'machine'    => $type->type,
            'updated_at' => Carbon::now(),
            'created_at' => Carbon::now()
        ]);

        return $this->respond($json);
    }
}
