@extends('app')

@section('content')
    <div class="col-xs-12">
        <div class="page-header">
            <h1 class="text-center">Rezultat iskanja @if(strlen($query)>0)"{{ $query }}"@endif</h1>
            @include('restaurants.partials.dropdown')
        </div>
    </div>
    <p class="col-xs-12">{{ $results->total() }} rezultatov</p>
    <div class="clearfix"></div>
    @foreach($results as $restaurant)
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            @include('restaurants.partials.restaurant_list')
        </div>
    @endforeach

    <div class="col-xs-12 text-center">{{ $results->appends(Request::except('page'))->links() }}</div>
@stop

@section('scripts')
    <script src="../assets/vendor/select/js/bootstrap-select.js"></script>
    <script>
        $(function () {
            $(document).on('change', 'select[name=order]', function (e) {
                window.location.replace("{!! generateUrl(url()->current(), Request::except('order'))  !!}&order="+ e.target.value);
            });
        })
    </script>
    @include('restaurants.partials.like_script')
@stop