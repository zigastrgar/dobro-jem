@extends('app')

@section('content')
    <div class="col-xs-12">
        <h1 class="page-header text-center">
            Prijave napak za Dobro jem
        </h1>
        <table class="table table-responsive table-bordered table-condensed">
            <tr>
                <th>Naslov</th>
                <th>Povezava</th>
                <th>Komentar</th>
                <th>Email</th>
                <th>Tip napake</th>
                <th>Uporabnik ID</th>
                <th>IP naslov</th>
                <th>HTTP agent</th>
            </tr>
            @foreach($reports as $report)
                <tr>
                    <td>{{ $report->title }}</td>
                    <td>{{ $report->link }}</td>
                    <td>{{ $report->comment }}</td>
                    <td>{{ $report->email }}</td>
                    <td> @if($report->type_id==1){{"Tehnična napaka"}}
                         @elseif($report->type_id==2){{"Pohvala/Predlog"}}
                         @elseif($report->type_id==3){{"Ime restavracije"}}
                        @elseif($report->type_id==4){{"Lokacija restavracije"}}
                        @elseif($report->type_id==5){{"Napaka v meniju"}}
                        @elseif($report->type_id==6){{"Slika"}}
                        @elseif($report->type_id==7){{"Neprimeren komentar"}}
                        @elseif($report->type_id==8){{"Neprimereno ime uporabnika"}}
                        @elseif($report->type_id==9){{"Slovnična napaka"}}
                        @elseif($report->type_id==10){{"Drugo"}}
                        @endif
                    </td>
                    <td>{{ $report->user_id }}</td>
                    <td>{{ $report->ip }}</td>
                    <td>{{ $report->agent }}</td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection

