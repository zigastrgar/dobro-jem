<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relations', function(Blueprint $table){
            $table->increments('id');
            $table->enum('result_type', ['restaurant', 'menu'])->default('restaurant');
            $table->integer('result_id')->unsigned();
            $table->integer('index_id')->unsigned();
            $table->foreign('index_id')->references('id')->on('indexes')->onUpdate('cascade')->onDelete('cascade');
            $table->string('restaurant_id');
            $table->foreign('restaurant_id')->references('original_id')->on('restaurants')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('relations', function(Blueprint $table){
            $table->dropForeign('relations_index_id_foreign');
            $table->dropForeign('relations_restaurant_id_foreign');
        });

        Schema::drop('relations');
    }
}
