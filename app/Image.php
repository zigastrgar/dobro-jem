<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = "images";

    protected $fillable = [
        'id',
        'primary',
        'restaurant_id',
        'url',
        'foursquare_id',
        'prefix',
        'suffix',
        'created_at',
        'updated_at'
    ];

    public function restaurant()
    {
        $this->hasOne('App\Restaurant');
    }
}
