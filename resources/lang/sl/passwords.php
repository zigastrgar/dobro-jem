<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Vaše geslo je bilo ponastavljeno.',
    'sent' => 'Na mail vam je bil poslan mail s povezavo za spremembo gesla!',
    'token' => 'Ključ za ponastavljanje gesla je napačen.',
    'user' => "Uporabnik s takšnim e-naslovom ne obstaja.",

];
