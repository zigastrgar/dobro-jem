<?php

namespace App\Console\Commands;

use App\Restaurant;
use Illuminate\Console\Command;

class Slug extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:slugs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks if every restaurant have unique slug. If don\'t, it fixes the problem.';

    /**
     * Create a new command instance.
     *
     * @param Restaurant $restaurant
     */
    public function __construct(Restaurant $restaurant)
    {
        parent::__construct();

        $this->restaurant = $restaurant;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $restaurants = Restaurant::whereRaw('slug = original_id')->get();

        foreach ( $restaurants as $restaurant ) {
            $slug = translateName($restaurant->name);

            if ( Restaurant::where('slug', $slug)->exists() ) {
                $slug .= "-" . translateName($restaurant->city);
            }

            if ( Restaurant::where('slug', $slug)->exists() ) {
                $slug .= "-1";
            }

            $restaurant->slug = $slug;
            $restaurant->save();
        }
    }
}
