<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApiKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_keys', function(Blueprint $table) {
            $table->increments('id');
            $table->string('owner');
            $table->string('key')->unique();
            $table->enum('level', [1, 2, 3, 4])->default(1);
            $table->enum('type', ['web', 'ios', 'android', 'windows'])->default('web');
            $table->timestamp('valid_until');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_keys');
    }
}
