<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Restaurant;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    public function add($restaurant, Request $request)
    {
        $restaurant = [ 'restaurant_id' => $restaurant ];
        $comment    = new Comment(array_merge($request->all(), $restaurant));
        Auth::user()->comments()->save($comment);
        $rest = Restaurant::where('original_id', $restaurant)->first();

        return redirect('restaurants/' . $rest->slug);
    }
}
