<h2 class="page-header text-center">Ocena restavracije</h2>
<div class="col-xs-12 text-center font-2x" id="overall">
    <p style="margin-top:5px;">{{ getRating($restaurant->ratings()->lists('rate')) }}</p>
    @if(count($restaurant->ratings->toArray()) > 0)
        <div style="display: none;" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
            <span itemprop="ratingValue">{{ getRating($restaurant->ratings()->lists('rate')) }}</span> glede na
            <span itemprop="reviewCount">{{ count(array_unique($restaurant->ratings()->lists('user_id')->toArray())) }}</span> ocen
        </div>
    @endif
    <p>
        {!! showStars(getRating($restaurant->ratings()->lists('rate'))) !!}
    </p>
</div>
<div class="col-xs-12 col-md-8 col-md-push-2">
    <div class="col-xs-6 col-sm-3 text-center center-block">
        <p>Kvaliteta</p>
        <?php
        $quality = didRateRestaurant(Auth::id(), $restaurant->original_id, "quality");
        $quality_num = getRating($restaurant->ratings()->where('rating_type_id', getRateTypeId("quality"))->lists('rate'));
        ?>
        <p title="{{ $quality_num }}" data-toggle="tooltip" data-placement="bottom" id="quality" @if($quality) class="font-1-3-x" @else class="starrr" @endif>@if($quality) {!! showStars($quality_num) !!} @endif</p>
    </div>
    <div class="col-xs-6 col-sm-3 text-center center-block">
        <p>Čistoča</p>
        <?php
        $clean = didRateRestaurant(Auth::id(), $restaurant->original_id, "clean");
        $clean_num = getRating($restaurant->ratings()->where('rating_type_id', getRateTypeId("clean"))->lists('rate'));
        ?>
        <p title="{{ $clean_num }}" data-toggle="tooltip" data-placement="bottom" id="clean" @if($clean) class="font-1-3-x" @else class="starrr" @endif>@if($clean) {!! showStars($clean_num) !!} @endif</p>
    </div>
    <div class="col-xs-6 col-sm-3 text-center center-block">
        <p>Postrežba</p>
        <?php
        $service = didRateRestaurant(Auth::id(), $restaurant->original_id, "service");
        $service_num = getRating($restaurant->ratings()->where('rating_type_id', getRateTypeId("service"))->lists('rate'));
        ?>
        <p title="{{ $service_num }}" data-toggle="tooltip" data-placement="bottom" id="service" @if($service) class="font-1-3-x" @else class="starrr" @endif>@if($service) {!! showStars($service_num) !!} @endif</p>
    </div>
    <div class="col-xs-6 col-sm-3 text-center center-block">
        <p>Ambient</p>
        <?php
        $ambient = didRateRestaurant(Auth::id(), $restaurant->original_id, "ambient");
        $ambient_num = getRating($restaurant->ratings()->where('rating_type_id', getRateTypeId("ambient"))->lists('rate'));
        ?>
        <p title="{{ $ambient_num }}" data-toggle="tooltip" data-placement="bottom" id="ambient" @if(didRateRestaurant(Auth::id(), $restaurant->original_id, "ambient")) class="font-1-3-x" @else class="starrr" @endif>@if($ambient) {!! showStars($ambient_num) !!} @endif</p>
    </div>
</div>
<div class="clearfix"></div>