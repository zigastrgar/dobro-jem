<?php

namespace App\Http\Middleware;

use App\Api;
use App\Log;
use Carbon\Carbon;
use Closure;

class checkAPILevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @param                           $role
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if ( !API::where('key', $request->input('api_key'))->where('level', '<', $role)->exists() ) {
            $arr = array();
            foreach($request->except(['api_key']) as $key => $val){
                $arr[] = $key."=".$val;
            }
            Log::create(['key' => $request->input('api_key'), 'page' => $request->url(), 'created_at' => Carbon::now(), 'update_at' => Carbon::now(), 'additional' => implode('&', $arr)]);
            return $next($request);
        } else {
            return abort(403, 'Za dostop do tega API-ja rabite višji level!');
        }
    }
}
