<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;

class LikeApiController extends ApiController
{
    public function like(Request $request)
    {
        if ( Auth::loginUsingId($request->input('user')) ) {
            if ( !Favourite::where('user_id', $request->input('user'))
                ->where('restaurant_id', $request->input('restaurant'))->exists()
            ) {
                Favourite::create([
                    'restaurant_id' => $request->input('restaurant'),
                    'user_id'       => $request->input('user')
                ]);
                $json["response"] = true;
                $json["type"]     = true;
                $json["message"]  = "Restavracija uspešno dodana med priljubljene!";
            } else {
                Favourite::where('restaurant_id', $request->input('restaurant'))
                    ->where('user_id', $request->input('user'))->delete();
                $json["response"] = true;
                $json["type"]     = false;
                $json["message"]  = "Restavracija uspešno odstranjena iz seznama priljubljenih!";
            }

            return $this->respond($json);
        }

        return $this->respondNotFound("Težava z uporabniškim ID-jem!");
    }
}
