<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RatingTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rating_types')->insert(['name' => 'Kvaliteta']);
        DB::table('rating_types')->insert(['name' => 'Čistoča']);
        DB::table('rating_types')->insert(['name' => 'Postrežba']);
        DB::table('rating_types')->insert(['name' => 'Ambient']);
    }
}
