@extends('app')

@section('content')
    <div class="col-xs-12">
        <h1 class="page-header text-center">Nadzorna plošča</h1>
        <a href="{{ url("/dashboard/users") }}">
            <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                <p class="font-4x icon icon-user"></p>
                <p>Uporabniki</p>
            </div>
        </a>
        <a href="{{ url('/dashboard/comments') }}">
            <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                <p class="font-4x icon icon-comment"></p>
                <p>Komentarji</p>
            </div>
        </a>
        <a href="{{ url('/dashboard/reports') }}">
            <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                <p class="font-4x icon icon-bubble-comment-streamline-talk"></p>
                <p>Prijave, pohvale, predlogi</p>
            </div>
        </a>
        <a href="{{ url('/dashboard/keywords') }}">
            <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                <p class="font-4x icon icon-search"></p>
                <p>Ključne besede</p>
            </div>
        </a>
        <a href="{{ url('/dashboard/statistics') }}">
            <div class="col-xs-12 col-sm-6 col-md-4 text-center">
                <p class="font-4x icon icon-bar-chart"></p>
                <p>Statistika</p>
            </div>
        </a>
    </div>
@endsection