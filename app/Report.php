<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = "reports";

    protected $fillable = [ 'title', 'link', 'comment', 'email', 'type_id', 'user_id', 'ip', 'agent' ];

    public function type()
    {
        return $this->hasOne('App\ReportType');
    }
}
