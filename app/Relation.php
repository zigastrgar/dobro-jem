<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relation extends Model
{
    protected $table = "relations";

    protected $fillable = [ 'restaurant_id', 'index_id', 'result_id', 'result_type' ];

    public function restaurants()
    {
        return $this->belongsTo('App\Restaurant', 'original_id', 'restaurant_id');
    }

    public function indexes()
    {
        return $this->belongsTo('App\Index', 'id', 'index_id');
    }
}
