<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeaturesRestaurantsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_restaurant', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('feature_id')->unsigned()->index();
            $table->string('restaurant_id');
            $table->foreign('feature_id')->references('id')->on('features')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('restaurant_id')->references('original_id')->on('restaurants')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('feature_restaurant', function(Blueprint $table){
            $table->dropForeign('feature_restaurant_restaurant_id_foreign');
            $table->dropForeign('feature_restaurant_feature_id_foreign');
        });
        Schema::drop('feature_restaurant');
    }
}
