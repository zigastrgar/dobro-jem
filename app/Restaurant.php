<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\Sluggable;

class Restaurant extends Model
{
    use Sluggable;


    protected static function boot()
    {
        parent::boot();

        static::created(function(Restaurant $restaurant) {
            $restaurant->index($restaurant);
        });

        static::updated(function(Restaurant $restaurant) {
            $restaurant->index($restaurant);
        });

        static::addGlobalScope('active', function(Builder $builder) {
            return $builder->getQuery()->where('hidden', false);
        });
    }

    protected $casts = [
        'lat'             => 'double',
        'lng'             => 'double',
        'value_of_charge' => 'float'
    ];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'restaurants';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'address',
        'city',
        'coordinates',
        'lat',
        'lng',
        'phone',
        'original_id',
        'opening_week',
        'opening_sat',
        'opening_sun',
        'opening_notes',
        'value_of_charge',
        'slug'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [ 'created_at', 'updated_at', 'hidden', 'slug', 'opening_note' ];

    protected $primaryKey = "original_id";


    /**
     * Returns all the comments which belongs to this restaurant
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment', 'restaurant_id', 'original_id');
    }

    /**
     * Returns all the comments which belongs to this restaurant and they are ordered by date
     *
     * @return mixed
     */
    public function commentsOrdered()
    {
        return $this->hasMany('App\Comment', 'restaurant_id', 'original_id')->orderBy('created_at', 'desc');
    }

    /**
     * Returns all the features which belongs to this restaurant
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function features()
    {
        return $this->belongsToMany('App\Feature', 'feature_restaurant')->withTimestamps();
    }

    /**
     * Returns all the daily menus of restaurant
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menus()
    {
        return $this->hasMany('App\Menu', 'restaurant_id', 'original_id');
    }

    /**
     * Returns all ratings of restaurant
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany('App\Rating', 'restaurant_id', 'original_id');
    }

    /**
     * Scope for querying specific rating of given restaurant
     *
     * @param $condition
     *
     * @return mixed
     * @internal param $query
     */
    public function rating($condition)
    {
        return $this->where('rating_type_id', getRateTypeId($condition))->lists('rate');
    }

    /**
     * Order by scope
     *
     * @param $query
     * @param $order
     *
     * @return mixed
     */
    public function scopeOrdering($query, $order)
    {
        if ( strlen($order) == 0 ) {
            $order = [ 0 => 'distance', 1 => 'asc' ];
        } else {
            $order = explode("_", $order);
            if ( $order[0] == "price" )
                $order[0] = "value_of_charge";
        }

        return $query->orderBy($order[0], $order[1]);
    }

    public function scopeDistance($query, $lat = null, $lng = null)
    {
        $lat = ( is_null($lat) ) ? ( Auth::check() ) ? deg2rad(Auth::user()->lat) : deg2rad(session('lat')) : $lat;
        $lng = ( is_null($lng) ) ? ( Auth::check() ) ? deg2rad(Auth::user()->lng) : deg2rad(session('lng')) : $lng;

        return $query->select(DB::raw('*, (acos(cos(' . $lat . ') * cos(radians(lat)) * cos(' . $lng . ' - radians(lng)) + sin(' . $lat . ') * sin(radians(lat))) * 6371000) as distance'));
    }

    public function favourites()
    {
        return $this->hasMany('App\Favourite', 'restaurant_id', 'original_id');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function primaryImage()
    {
        return $this->hasOne('App\Image')->where('primary', true);
    }

    public function index()
    {
        $now = Carbon::now();
        try {
            if ( !Index::where('content', translate($this->name))->exists() )
                Index::create([
                    'content'    => translate($this->name),
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
            if ( !Index::where('content', translate($this->city))->exists() )
                Index::create([
                    'content'    => translate($this->city),
                    'created_at' => $now,
                    'updated_at' => $now
                ]);
        } catch ( QueryException $ex ) {

        }
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
}
