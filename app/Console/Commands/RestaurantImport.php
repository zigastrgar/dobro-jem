<?php

namespace App\Console\Commands;

use App\Feature;
use App\Jobs\CheckForOutdatedRestaurants;
use App\Jobs\FixRestaurantData;
use App\Menu;
use App\Restaurant;
use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;

class RestaurantImport extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:restaurants';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports restaurants from bonar.si';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = json_decode(file_get_contents("http://bonar.si/api/restaurants"), true);

        foreach ( $data as $rest ) {

            $restaurantData = [
                'name'            => $rest['name'],
                'value_of_charge' => (float)str_replace(",", ".", $rest['price']),
                'opening_week'    => ( $rest['opening']['week'] != false ) ? implode(",", $rest['opening']['week']) : null,
                'opening_sat'     => ( $rest['opening']['saturday'] != false ) ? implode(",", $rest['opening']['saturday']) : null,
                'opening_sun'     => ( $rest['opening']['sunday'] != false ) ? implode(",", $rest['opening']['sunday']) : null,
                'opening_note'    => ( array_key_exists("notes", $rest['opening']) ) ? $rest['opening']['notes'] : null,
                'phone'           => implode(",", $rest['telephone']),
                'address'         => explode(", ", $rest['address'])[0],
                'city'            => explode(", ", $rest['address'])[1],
                'lat'             => $rest['coordinates'][0],
                'lng'             => $rest['coordinates'][1],
                'original_id'     => $rest["id"],
                'hidden'          => false
            ];
            $restaurant     = Restaurant::withoutGlobalScope('active')->where('original_id', $rest["id"])->first();
            if ( is_null($restaurant) ) {
                Restaurant::create($restaurantData);
                $this->line('Vstavljam: ' . $rest['name'] . ' ' . $rest['id']);
            } else {
                $restaurant->update(array_except($restaurantData, [ 'slug' ]));
                $this->line('Posodabljam: ' . $rest['name'] . ' ' . $rest['id']);
            }

            $this->menus($rest['menu'], $rest['id']);

            if ( array_key_exists("features", $rest) ) {
                $this->features($rest['features'], $rest['id']);
            }
        }

        dispatch(new FixRestaurantData);
        dispatch(new CheckForOutdatedRestaurants);
    }

    private function menus(array $menus, $id)
    {
        DB::table('menus')->where('restaurant_id', $id)->delete();

        foreach ( $menus as $menu ) {
            foreach ( $menu as $index => $food ) {
                $menu[$index] = preg_replace('/[0-9]+\./', "", trim($food, ",.:;"));
            }
            $men = implode('(|)', $menu);
            Menu::create([
                'content'       => $men,
                'restaurant_id' => $id,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ]);
        }
        $this->line('Meniji vstavljeni za: ' . $id);
    }

    private function features(array $features, $id)
    {
        $table = [
            1  => 2, // Vegi meni
            3  => 8, // Dostopno invalidom
            9  => 3, // Študentske ugodnosti
            11 => 5, // Kosila
            8  => 8, // Invalidom dostopno
            10 => 7, // Odprto med vikendi
            12 => 1, // Pizze
            5  => 6, // Dostava
            13 => 9, // hitra hrana
            7  => 4 // Solate
        ];

        $f_array = [];
        foreach ( $features as $feature ) {
            $f_array[] = $table[$feature['id']];
        }

        Restaurant::where('original_id', $id)->first()->features()->sync($f_array);

        $this->line('Lastnosti restavracije posodobljene: ' . $id);
    }
}
