<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        //$this->call(RatingTypesSeeder::class);
        //$this->call(FeaturesSeeder::class);
        //$this->call(ApiKeys::class);
        //$this->call(ReportTypesSeeder::class);

        Model::reguard();
    }
}
