@extends('app')

@section('content')
    <h1 class="page-header text-center">Urejanje profila</h1>
    <div class="col-xs-12 col-md-8 col-md-push-2">
        {!! Form::model(Auth::user(),['url' => 'editProfile']) !!}
        <div class="form-group col-xs-12 col-md-6">
            {!! Form::label('name', 'Ime in priimek') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-xs-12 col-md-6">
            {!! Form::label('email', 'E-naslov') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
        <div class='form-group col-xs-12 col-md-6'>
            {!! Form::label('username', 'Uporabniško ime') !!}
            {!! Form::text('username', null, ['class' => 'form-control']) !!}
            <p class="help-block">Če ne boste imeli vnešenega uporabniškega imena, bo ob izpisu komentarjev restavracije
                uporabljen vaš e-naslov.</p>
        </div>
        <div class='form-group  col-xs-12 col-md-6'>
            {!! Form::label('address', 'Naslov') !!}
            {!! Form::text('address', null, ['class' => 'form-control']) !!}
            <p class="help-block">Naslov bo uporabljen na zemljevidu na <a href="{{ url('nearest') }}">strani</a> za
                iskanje restavracij, ki so Vam najbližje in za izračun oddaljenosti do restavracije. Primer: Slovenska cesta 58, Ljubljana</p>
        </div>
        <div class="col-xs-12">
            <input type="submit" class="btn btn-success" value="Shrani podatke">
        </div>
        {!! Form::close() !!}
    </div>
@stop