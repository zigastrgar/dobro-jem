@extends('home')

@section('content')
    <div class="col-xs-12 col-md-6 col-md-push-3">
        <div class="col-xs-12">
            <div style="margin-top: 30px;" id="rest">
                <div class="hidden-xs">
                    <a href="{{ url('auth/facebook') }}" class="btn btn-lg btn-social btn-facebook">
                        <span class="icon icon-facebook"></span> Facebook
                    </a>
                    <a href="{{ url('auth/twitter') }}" class="btn btn-lg btn-social btn-twitter">
                        <span class="icon icon-twitter"></span> Twitter
                    </a>
                    <a href="{{ url('auth/github') }}" class="btn btn-lg btn-social btn-github">
                        <span class="icon icon-github"></span> GitHub
                    </a>
                </div>
                <div class="visible-xs">
                    <a href="{{ url('auth/facebook') }}" class="btn btn-lg btn-social-icon btn-facebook">
                        <span class="icon icon-facebook"></span>
                    </a>
                    <a href="{{ url('auth/twitter') }}" class="btn btn-lg btn-social-icon btn-twitter">
                        <span class="icon icon-twitter"></span>
                    </a>
                    <a href="{{ url('auth/github') }}" class="btn btn-lg btn-social-icon btn-github">
                        <span class="icon icon-github"></span>
                    </a>
                </div>
                <h3 class="text-center">ali</h3>
            </div>
            <div class="clearfix"></div>
            <div id="mail" class="form-group form-group-lg" style="margin-top: 20px;">
                <div class="input-group">
                    <input type="email" name="email" class="form-control normal-line-height" placeholder="E-naslov"
                           required/>
                        <span class="input-group-btn">
        <button class="btn btn-lg btn-success" id="next" type="button">Naprej <span
                    class="icon icon-angle-right"></span></button>
      </span>
                </div>
                {!! csrf_field() !!}
                <div id="result"></div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script>
        $("#next").click(function () {
            var mail = $("input[name=email]").val();
            if (validateEmail(mail)) {
                $.ajax({
                    url: '/checkEmail',
                    type: 'POST',
                    data: {email: mail, _token: $("input[name=_token]").val()},
                    success: function (cb) {
                        cb = cb.split("&%");
                        if (cb[0] == "register") {
                            register(cb[1].split("|"));
                        } else if (cb[0] == "login") {
                            login(cb[1].split("|"));
                        } else {
                            social(cb[1].split("|"))
                        }
                    }
                });
            }
        });

        function social(data) {
            $("#result").html(data[0]);
        }

        function register(data) {
            $("#mail").prepend(data[0]);
            $("#result").html(data[1]);
            $("#rest").toggle('slide');
            $("#next").remove();
            $("#mail").children().removeClass('input-group');
        }

        function login(data) {
            $("#mail").prepend(data[0]);
            $("#result").html(data[1]);
            $("#rest").toggle('slide');
            $("#next").remove();
            $("#mail").children().removeClass('input-group');
        }

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        $(document).on('click', '#go', function () {
            var data;
            if ($(this).attr("action") == "register") {
                data = {
                    email: $("input[name=email]").val(),
                    password: $("input[name=password]").val(),
                    password1: $("input[name=password1]").val(),
                    _token: $("input[name=_token]").val()
                };
            } else {
                data = {
                    email: $("input[name=email]").val(),
                    password: $("input[name=password]").val(),
                    _token: $("input[name=_token]").val()
                };
            }
            $.ajax({
                url: '/loginOrRegister',
                type: 'POST',
                data: data,
                success: function (cb) {
                    cb = cb.split("|");
                    if (cb[0] == "success") {
                        if (cb[1] == "true") {
                            location.reload()
                        } else if (cb[1] == "first_time") {
                            location.href = "/editProfile";
                        }
                    }
                    if (cb[0] == "error") {
                        console.log(cb[1]);
                    }
                }
            })
        });
    </script>
@stop